class AppConfig{
   static selfUrl = "http://localhost:3000"
   static client_id = "react_UI";   //application id for identity server
   static refreshTokenTimeOut = 1800000; //30 минут (на сервере время жизни токена установлено на 1 час)
   
   static identityServerUrl = "https://localhost:50001";
   static usersUrl = "https://localhost:7040";
   static chatServerUrl = "https://localhost:7119";
   static filesUrl = "https://localhost:7202";
}

export default AppConfig;