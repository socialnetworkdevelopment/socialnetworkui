import React, { useState } from "react";
import axios from "axios";
import { useDispatch } from 'react-redux';
import AuthenticationManager from "../login/oidc_AuthenticationManager";
import ImgAutoLoad from "../components/imgAutoLoad";
import ButtonUploadImg from "../components/uploaderFiles/buttonUploadImg";

const LogoutPage = function() {
    const [imgId,setImgId] = useState()

    const dispatch = useDispatch();

    const webAppRequest = async () => {
        let url = "https://localhost:50005/api/get";
        axios.get(url).then((response) => {
            console.log(response);
        });
    }

    const logOut = () => {
        AuthenticationManager().signoutRedirect();
        dispatch({ type: 'UPDATE_TOKEN', payload: '' });
    }

    const showUser = async () => {
        AuthenticationManager().getUser().then((u) => {
            console.log(u);
        });
    }

    const refreshToken = ()=>{
        AuthenticationManager().refreshToken(dispatch,true);
    }


    // Обработка прогресса загрузки файла
    const callBackProgress = (percent)=>{
        console.log(`Загружено ${percent}%`);
    }
    // Получение id загруженного файла из БД
    const callBackUploadResult = (newIdImage)=>{
        console.log("Id загруженного изображения: "+newIdImage);
        
        setImgId(newIdImage);

    }

    return (
        <div style={{background:"#36393f",padding:"3em"}}>
            <button onClick={webAppRequest}>WebApp</button>
            <button onClick={logOut}>LogOut</button>
            <button onClick={showUser}>Show User</button>
            <button onClick={refreshToken}>RefreshToken</button>

            <div style={{overflow:"hidden",width:"3.5em",height:"3.5em",borderRadius:"100%",margin:"2em"}}>
                <ImgAutoLoad id={imgId} style={{width: "100%",height: "100%", objectFit:"cover"}}></ImgAutoLoad>
            </div>
            <ButtonUploadImg result={callBackUploadResult} progress={callBackProgress}></ButtonUploadImg>
        </div>
    );
}
export default LogoutPage;