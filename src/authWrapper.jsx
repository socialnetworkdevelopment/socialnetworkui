import React, { useEffect, useState } from 'react';
import AuthenticationManager from './login/oidc_AuthenticationManager';
import { useDispatch } from 'react-redux';
import { useLocation } from 'react-router-dom';

const AuthWrapper = ({ children }) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const [intervalId,setIntervalId] = useState(0);

  useEffect(() => {
    if(!["login","registration","callback","logout"].includes(location.pathname.split("/")[1]))
    {
      clearInterval(intervalId);
      setIntervalId(AuthenticationManager().startAutoRefreshToken(dispatch));
    }
  }, [location]);

  return (
    <>
      {children}
    </>
  );
}

export default AuthWrapper;