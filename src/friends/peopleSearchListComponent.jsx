import axios from "axios";
import React, { useEffect, useState } from "react";
import AppConfig from "../AppConfig";
import { store } from "../store";
import PeopleSearchComponent from "./peopleSearchComponent";

const PeopleSearchListComponent = ()=>{
    const [state,setState] = useState({people:[]})
    useEffect(()=>{
    },[])

    //Отправка запроса на добавление в друзья
    const addFriend = (user)=>{
        let selfId =store.getState().session.currentUserId;
        let data = {
            senderId: selfId,
            receiverId: user.id,
            type: 1};

        axios.post(AppConfig.usersUrl + "/api/relationshipRequest",data)
        .then((response)=>{
            if(response.status === 201)
                window.alert("Запрос отправлен");
        });
    }

    //Поиск пользователей
    const searchStringChanging = (event) => {
        let searchString = event.currentTarget.value;
        while(searchString.includes("  "))
            searchString = searchString.replace("  "," ");

        event.currentTarget.value = searchString;
        
        if(searchString != "" && searchString != null && searchString != undefined)
        axios.get(AppConfig.usersUrl+`/api/users/search/${searchString}`)
        .then(response=>{
            setState({...state,people:response.data})
        })
    }

    let showPeopleList = state.people.map(item=>{
        return <PeopleSearchComponent user={item} addFriend={addFriend}/>
    })
    if(state.people.length == 0)
        showPeopleList = <label className="people-search-list-component-not-found-label">no people found...</label>;

    return (<div className="people-search-list-component-main-container">
        <input placeholder="Find new friends..." className="people-search-list-component-input" onChange={searchStringChanging}></input>
        {showPeopleList}
    </div>);
}

export default PeopleSearchListComponent;