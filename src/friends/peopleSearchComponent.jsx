import React, { useEffect, useState } from "react";
import ImgAutoLoad from "../components/imgAutoLoad";
import { store } from "../store";

const PeopleSearchComponent = (props)=>{
    const [state,setState] = useState({})

    const addFriend = ()=>{
        props.addFriend(props.user);
    }

    return (
    <div className="friends-common-container-item">
        <a href={`/profile/${props.user.id}`} className="friendComponentAvatarContainer">
            <ImgAutoLoad id={props.user.avatarId}/>
        </a>
        <div className="relationship-request-item-label-container">
            <label className="friendComponentLabelName">{props.user.firstName} {props.user.lastName}</label>
        </div>
        {props.user.id == store.getState().session.currentUserId?
        <label className="people-search-self-user-label">It is you</label>
        :
        <button className="button-inherit" onClick={addFriend}>Add friend</button>
        }
    </div>);
}

export default PeopleSearchComponent;