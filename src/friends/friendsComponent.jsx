import React from "react";
import { useParams } from "react-router";
import './friends.css';
import ButtonOpenChatByFriendId from "../components/buttonOpenChatByFriendId";
import ImgAutoLoad from "../components/imgAutoLoad";
import ButtonRemoveFromFriends from "../components/buttonRemoveFromFriends";

const FriendsComponent = ({user,removeFriendsCallBack})=>{
    return(
        <div className="friends-common-container-item">
            <a href={`/profile/${user.id}`} className="friendComponentAvatarContainer">
                <ImgAutoLoad id={user.avatarId}/>
            </a>

            <div className="friendComponentLabelContainer">
                <a href={`/profile/${user.id}`} className="friendComponentLabelName">{user.firstName} {user.lastName}</a>
                <label className="friendComponentLabelStatus">your friend</label>
            </div>
            <ButtonOpenChatByFriendId friendId={user.id}>Message</ButtonOpenChatByFriendId>
            <ButtonRemoveFromFriends friendId={user.id} successCallBack={removeFriendsCallBack}>
                <button className="button-inherit friendComponentButtonDelete">
                        <img/>  
                </button>
            </ButtonRemoveFromFriends>
        </div>
    );
}

export default FriendsComponent;