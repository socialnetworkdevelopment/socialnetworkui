import React, { useState } from "react";
import './friends.css'
import AppConfig from "../AppConfig";
import axios from "axios";
import { store } from "../store";
import ImgAutoLoad from "../components/imgAutoLoad";

const FriendsPossibleComponent = (props) =>{
    const [state,setState] = useState({
        user:props.user,
        isInvited:false
    })

    const relationshipRequest = ()=>{
        let selfId =store.getState().session.currentUserId;
        let data = {
            senderId: selfId,
            receiverId: state.user.id,
            type: 1};

        axios.post(AppConfig.usersUrl + "/api/relationshipRequest",data)
        .then((response)=>{
            if(response.status === 201)
                setState({...state,isInvited:true});
        });
    }

        
        let buttonAddFriend;
        if(!state.isInvited)
            buttonAddFriend = <button onClick={relationshipRequest} className="button-simple friend-possible-component-item-add-button">+ add friend</button>;
        else
            buttonAddFriend = <label className="friend-possible-component-invite-status">invited</label>


        return(<div className="friendPossibleComponentContainer">
            <div className="friendComponentAvatarContainer">
                <ImgAutoLoad id={props.user.avatarId}/>
            </div>
            <div className="friendComponentLabelContainer">
                <label className="friend-possible-component-item-label">{props.user.firstName} {props.user.lastName}</label>
                {buttonAddFriend}
            </div>
        </div>);

}

export default FriendsPossibleComponent;