import { store } from '../store';
import React, { useEffect, useState } from "react";
import AppConfig from "../AppConfig";
import axios from "axios";
import FriendsComponent from './friendsComponent';

const FriendsListComponent = ()=>{
    const [state,setState] = useState({
        friends:[],
        searchStr:"",
    })

    useEffect(()=>{
        const userId = store.getState().session.currentUserId;

        Promise.all([
            axios.get(AppConfig.usersUrl + `/api/users/${userId}/friends`),
            axios.get(AppConfig.usersUrl + `/api/users/${userId}/friends`),
        ])
        .then((results)=>{
            setState({...state,
                friends:results[0].data["friends"],
                possibleFriends:results[1].data["friends"]})
        })
    },[])
    
    const searchFriends = (e)=>{
        setState({...state,searchStr:e.target.value});
    }

    const removeFriend = (friendId) => {
        console.log(friendId);
        setState({...state,friends:state.friends.filter(f=>f.id != friendId)})
    }


    return (
    <div>
        <h3>My friends</h3>
    
        <input placeholder="Search..." value={state.searchStr} onChange={searchFriends}></input>
        {state.friends.map((item)=>{
            if(item.firstName?.toUpperCase().includes(state.searchStr.toUpperCase()) || 
            item.lastName?.toUpperCase().includes(state.searchStr.toUpperCase()))
                return <FriendsComponent key={item.id} user={item} removeFriendsCallBack={removeFriend}/>
        })}
    </div>
    )
}

export default FriendsListComponent;