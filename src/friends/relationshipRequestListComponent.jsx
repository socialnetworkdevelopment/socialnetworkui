import React, { useEffect, useState } from "react";
import axios from "axios";
import AppConfig from "../AppConfig";
import { store } from '../store';
import RelationshipRequestComponent from "./relationshipRequestComponent";

const RelationshipRequestListComponent = ()=>{
    const [state,setState] = useState({
        requests:[]
    })
    useEffect(()=>{
        const id = store.getState().session.currentUserId;    
        axios.get(AppConfig.usersUrl+`/api/relationshipRequest/incomingByUser?userId=${id}`)
        .then((res)=>{
            setState({...state,requests:res.data.relationshipRequests});
        })
    },[])

    const relationshipAcceptReject=(request,status)=>{
        let url = "";
        
        if(status == "accept")
            url=AppConfig.usersUrl+`/api/relationshipRequest/accept/${request.id}`
        else if(status == "reject")
            url=AppConfig.usersUrl+`/api/relationshipRequest/reject/${request.id}`

        axios.put(url).then((res)=>{
            if(res.status === 204)
            {
                let arr = state.requests.filter((item)=>{
                    return item.id !== request.id;
                })
                setState({...state,requests:arr});
            }
            console.log(res);   
        })        
    }

    return(
        <div>
            {state.requests.map((item)=>{
                return <RelationshipRequestComponent request={item} delegate={relationshipAcceptReject}/>
            })}
        </div>
    );
}

export default RelationshipRequestListComponent;