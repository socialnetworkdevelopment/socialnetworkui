import React from "react";
import { useParams } from "react-router";
import './friends.css';
import ImgAutoLoad from "../components/imgAutoLoad";

const RelationshipRequestComponent = (props)=>{

    const relationshipButtonClick=(event)=>{
        props.delegate(props.request,event.currentTarget.dataset.value)
    }

    const createAt = new Date(props.request.createAt);
    
    return(<div className="friends-common-container-item">
        <a href={`/profile/${props.request.sender.id}`} className="friendComponentAvatarContainer">
            <ImgAutoLoad id={props.request.sender.avatarId}/>
        </a>
        <div className="relationship-request-item-label-container">
            <label className="friendComponentLabelName">{props.request.sender.firstName} {props.request.sender.lastName}</label>
            <label className="relationship-request-component-date">{createAt.toLocaleDateString()} {createAt.toLocaleTimeString()}</label>
        </div>
        <button className="button-inherit" data-value='accept' onClick={relationshipButtonClick}>Apply</button>
        <button className="button-inherit relationship-request-component-button-cansel" data-value='reject' onClick={relationshipButtonClick}>
            <img></img>
        </button>
    </div>);
}

export default RelationshipRequestComponent;