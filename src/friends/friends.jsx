import React, { useEffect, useState } from "react";
import axios from "axios";
import AppConfig from "../AppConfig";
import { store } from '../store';

import './friends.css'
import FriendsPossibleComponent from "./friendsPossibleComponent";
import FriendsListComponent from "./friendsListComponent";
import NavigationComponent from "../components/navigation/navigation";
import RelationshipRequestListComponent from "./relationshipRequestListComponent";
import PeopleSearchListComponent from "./peopleSearchListComponent";
import Header from "../components/header/header";

const FriendsPage = ()=>{
    const [state,setState] = useState({
        friends:[],
        possibleFriends:[],
        searchStr:"",
        navigationId:0
    })

    useEffect(()=>{
        const userId = store.getState().session.currentUserId;
            axios.get(AppConfig.usersUrl + `/api/users/recommendations/${userId}?count=5`)
            .then((results)=>{
                setState({...state,
                    // friends:results[0].data["friends"],
                    possibleFriends:results.data})
            })
    },[]);


    let showComponent;
    let styleButton0 = "friends-navigation-item"
    let styleButton1 = "friends-navigation-item"
    let styleButton2 = "friends-navigation-item"
    let styleButton3 = "friends-navigation-item"
    switch(state.navigationId)
    {
        case 0:
            showComponent = <FriendsListComponent/>;
            styleButton0 = "friends-navigation-item friends-navigation-item-selected"
            break;
        case 1:
            showComponent = <RelationshipRequestListComponent/>;
            styleButton1 = "friends-navigation-item friends-navigation-item-selected"
            break;
        case 2:
            showComponent = 2;
            styleButton2 = "friends-navigation-item friends-navigation-item-selected"
            break;
        case 3:
            showComponent = <PeopleSearchListComponent/>;
            styleButton3 = "friends-navigation-item friends-navigation-item-selected"
            break;
        default:
            showComponent = <FriendsListComponent/>;
            styleButton0 = "friends-navigation-item friends-navigation-item-selected"
            break;
    }

    const navigationChange = (e)=>{
        let num = Number.parseInt(e.target.dataset.num);
        setState({...state,navigationId:num})
    }
    
    return(<div className="common-background-content-page">
        <Header/>
        <div className="common-content-page-container">
            <NavigationComponent />
            <div className="friendsListContainer">
                <div className="friends-navigation-item-container">
                    <a className={styleButton0} data-num="0" onClick={navigationChange}>Friends</a>
                    <a className={styleButton1} data-num="1" onClick={navigationChange}>Incoming </a>
                    {/* <a className={styleButton2} data-num="2" onClick={navigationChange}>Outgoing</a> */}
                    <a className={styleButton3} data-num="3" onClick={navigationChange}>People search</a>
                </div>
                {showComponent}
            </div>
            {
            state.possibleFriends.length > 0 ?
                <div className="friends-possible-listcontainer">
                    <h3>People you may know</h3>
                    {state.possibleFriends.map((item)=>{return <FriendsPossibleComponent key={item.id} user={item} />})}
                </div>
            :""
            }
        </div>
    </div>);
}

export default FriendsPage;