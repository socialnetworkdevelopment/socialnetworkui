import React, { useEffect, useState } from "react";
import NavigationComponent from "../components/navigation/navigation";
import NotificationContainerComponent from "../components/notification/notificationContainerComponent";
import NotificationComponent from "../components/notification/notificationComponent";

const MainPage = ()=>{


    return(<div className="common-background">
        {/* <NotificationContainerComponent/> */}
        <NavigationComponent/>
        <div className="common-content-container">
            <ul>
            <li><a href='/login'>login</a></li>
            <li><a href='/registration'>registration</a></li>
            <li><a href='/logout'>logout</a></li>
            <li><a href='/friends'>friends</a></li>
            </ul>
        </div>
    </div>);
}

export default MainPage;