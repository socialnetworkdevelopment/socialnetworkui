import React, { Suspense, lazy, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route, useLoaderData, useLocation } from 'react-router-dom';
import './css/main.css';
import withSignalRConnections from './signalRConnectionWrapper';
import NotificationContainerComponent from './components/notification/notificationContainerComponent';
import AuthWrapper from './authWrapper';

const Loading = lazy(() => import('./loading/loading'));
const Login = lazy(() => import('./login/login'));
const Registration = lazy(() => import('./registration/registration'));
const CallBack = lazy(() => import('./login/callback'));
const Logout = lazy(() => import('./logout/logout'));
const Main = lazy(()=>import('./main/main'));
const Friends = lazy(()=>import('./friends/friends'));
const AllChats = lazy(() => import('./chat/allChats/allChats'));
const SingleChat = lazy(() => import('./chat/singleChat/singleChat'));
const Profile = lazy(() => import('./profile/profile'));
const CreateGroupChat = lazy(() => import('./chat/groupChat/createGroupChat'));
const Posts = lazy(() => import('./posts/Posts'));
const PostIdPage = lazy(() => import('./posts/PostIdPage'));
const ProfileSettings = lazy(()=> import('./profileSettings/profileSettings'));

const News = lazy(() => import('./posts/News'));

function App() {
  return (
    <Router>
      <Suspense fallback={<Loading />}>
        <NotificationContainerComponent/>
        <AuthWrapper>
          <Routes>
            <Route path='/' element={<Profile />} /> 
            <Route path='/friends' element={<Friends/>} />
            <Route path='/login' element={<Login />} />
            <Route path='/registration' element={<Registration />} />
            <Route path='/callback' element={<CallBack />} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/allChats' element={<AllChats />} />
            <Route path='/chat/:id' element={<SingleChat />} />
            <Route path='/profile' element={<Profile />} />
            <Route path='/profile/:id' element={<Profile />} />
            <Route path='/settings' element={<ProfileSettings/>}/>
            <Route path="/posts" element={<Posts/>} />
            <Route path="/posts/:id" element={<PostIdPage/>} />
            <Route path='/createGroupChat' element={<CreateGroupChat />}/>
            <Route path="/news" element={<News/>} />

            <Route path='/test' element={<Main />} />
          </Routes>
        </AuthWrapper>
      </Suspense>
    </Router>
  );
}

export default withSignalRConnections(App);