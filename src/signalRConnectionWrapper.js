import { connect } from 'react-redux';
import { chatSignalRService } from './chat/chatSignalRService.js';

function withSignalRConnections(Component) {
  function connectToSignalRHubs(state) {
    chatSignalRService.startConnection();
    return { token: state.session.token };
  }

  return connect(connectToSignalRHubs)(Component);
}

export default withSignalRConnections;