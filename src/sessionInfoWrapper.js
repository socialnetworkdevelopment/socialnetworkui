import { connect } from 'react-redux';

function withSessionInfo(Component) {
  function mapStateToProps(state) {
    return {
      token: state.session.token,
      currentUserId: state.session.currentUserId
    };
  }

  return connect(mapStateToProps)(Component);
}

export default withSessionInfo;