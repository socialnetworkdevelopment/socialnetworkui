import React, { useEffect } from 'react';
import { UserManager } from 'oidc-client';
import { useDispatch } from 'react-redux';
import { Buffer } from 'buffer';

const CallBack = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        const um = new UserManager({ response_mode: 'query' });

        um.signinRedirectCallback()
            .then((data) => {
                dispatch({
                    type: 'UPDATE_TOKEN',
                    payload: { token: data['access_token'], currentUserId: JSON.parse(Buffer.from(data['access_token'].split('.')[1], "base64").toString()).sub }
                });
                window.location.replace('/');
            })
            .catch((e) => {
                window.location.replace('/login');
            });
    }, []);

    return <div className="common-background"></div>;
};

export default CallBack;