import React, { useState, useEffect } from 'react';
import './login.css';
import axios from 'axios';
import AuthenticationManager from './oidc_AuthenticationManager';
import AppConfig from '../AppConfig';

const LoginPage = () => {
  
  const signIn = (e) => {
    e.preventDefault();
    const form = document.forms.signIn;
    const l = form.login.value;
    const p = form.password.value;

    let utl = `${AppConfig.identityServerUrl}/auth/login?loginName=${l}&password=${p}`;

    axios
      .get(utl, { withCredentials: true })
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          AuthenticationManager().signinRedirect();
        }
      })
      .catch((err) => {
        console.log(err);
        if (err.response.status === 404 || err.response.status === 401)
          window.alert(err.response.data);
      });
  };

  return (
    
    <div className='common-background'>
      <div className='common-content-container'>
        <form name='signIn'>
          <h1>
            <label>Social</label>
            <label className='common-header-word-text-color'>Buddy</label>
            <label>Chat</label>
          </h1>
          <h2>Sign in</h2>
          <div className='common-input-container'>
            <label>Login</label>
            <input placeholder='login or email' name='login' required />
          </div>
          <div className='common-input-container'>
            <label>Password</label>
            <input placeholder='password' type='password' name='password' required />
          </div>
          <div className='loginPageButtonSignInContainer'>
            <a href='/forgotPassword'>Forgot your password?</a>
            <button onClick={signIn}>
              Sign in
            </button>
          </div>
          <a className='loginPageRegistrationButton' href='/registration'>
            Register now!
          </a>
        </form>
      </div>
    </div>
  );
};

export default LoginPage;