import { UserManager } from 'oidc-client';
import AppConfig from '../AppConfig';
import { Buffer } from 'buffer';

const AuthenticationManager = () => {
  const oidcConfig = {
    authority: AppConfig.identityServerUrl,
    client_id: AppConfig.client_id,
    redirect_uri: AppConfig.selfUrl + "/callback",
    response_type: "code",
    scope: "openid profile offline_access",
    post_logout_redirect_uri: AppConfig.selfUrl + "/login",
  };
  const userManager = new UserManager(oidcConfig);

  const getUser = () => userManager.getUser();
  const signinRedirect = () => userManager.signinRedirect();
  const signoutRedirect = () => userManager.signoutRedirect();

  /**
   * Автообновление токена досупа (первое обновление при вызове метода, далее через интервал времени)
   * @param {*} dispatch результат хука useDispatch() для получения доступа к redux store, и обновления токена 
   * @param {*} refreshTimeOut (не обязательный) интервал для автообновления токена (по умолчанию из конфига AppConfig.refreshTokenTimeOut)
   * @param {*} debug (не обязательный) флаг отладки (по уполчанию false)
   */
  const startAutoRefreshToken = (dispatch, refreshTimeOut = AppConfig.refreshTokenTimeOut, debug = false) => {
    refreshToken(dispatch);
    return setInterval(() => {
      refreshToken(dispatch);
    }, refreshTimeOut);
  };

  const refreshToken = (dispatch, debug = false) => {
    console.log("REFRESH TOKEN");
    userManager
      .signinSilent({silentRequestTimeout:10})
      .then((data) => {
        dispatch({
          type: 'UPDATE_TOKEN',
          payload: {
            token: data['access_token'],
            currentUserId: JSON.parse(Buffer.from(data['access_token'].split('.')[1], "base64").toString()).sub
          }
        });
      })
      .catch((e) => {
        if (debug)
          console.log(e);
        else
          window.location.replace('/login');
      });
  }


  return {
    getUser,
    signinRedirect,
    signoutRedirect,
    startAutoRefreshToken,
    refreshToken
  };
}

export default AuthenticationManager;