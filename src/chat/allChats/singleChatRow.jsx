import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Moment from 'moment';
import axios from 'axios';
import AppConfig from '../../AppConfig';
import { chatSignalRService } from "../chatSignalRService.js";
import ImgAutoLoad from '../../components/imgAutoLoad';

function SingleChatRow({ chat, currentUserId, setChats, sortingChats, onRecieveMessageHandler }) {
    const chatMember = chat.chatMembers.filter(chatMember => chatMember.userId != currentUserId)[0];
    const currentUserChatMember = chat.chatMembers.filter(chatMember => chatMember.userId == currentUserId)[0];

    const [unreadMessagesCount, setUnreadMessagesCount] = useState(0);
    const [lastMessage, setLastMessage] = useState({ text: chat.lastMessage?.text, sendAt: chat.lastMessage?.sendAt, authorId: chat.lastMessage?.authorId })

    const onReceiveMessage = (authorId, chatId, messageId, messageText, sendAt) => {
        if (chatId == chat.id) {
            //onRecieveMessageHandler(authorId, chatId, messageId, messageText, sendAt)
            setChats(prevChats => {
                let currentChat = prevChats.filter(prevChat => prevChat.id == chat.id)[0];
                let newMessage = { text: messageText, id: messageId, authorId: currentUserId, sendAt: sendAt };
                currentChat.lastMessage = newMessage;
                return sortingChats([...prevChats]);;
            })
            setUnreadMessagesCount(prevValue => prevValue + 1);
            setLastMessage({ text: messageText, sendAt: sendAt });
        }
    }

    useEffect(() => {
        axios.get(`${AppConfig.chatServerUrl}/api/chats/${chat.id}/unreadMessagesCount?userId=${currentUserId}`)
            .then((response) => {
                setUnreadMessagesCount(response.data);
            })
            .catch((error) => {
                console.error('Error fetching chats:', error);
            });

        chatSignalRService.onReceiveMessage(onReceiveMessage);

        return () => {
            chatSignalRService.offReceiveMessage(onReceiveMessage);
        }
    }, [])

    return (
        <Link to={`/chat/${chat.id}`}>
            <div className='single-chat-row'>

                <div className='single-chat-group-frame'>
                    <div className='single-chat-user-image-container'>
                        {
                            chat.type == 0 ?
                                <ImgAutoLoad id={chatMember.avatarId} className={'single-chat-user-image'} />
                                :
                                <ImgAutoLoad id={chat.imgId} className={'single-chat-user-image'} />
                        }

                    </div>
                    <div className='single-chat-user-info-group'>
                        <div className='single-chat-user-name'>
                            <div className='single-chat-user-name-text'>
                                {chat.type == 0 ? chatMember.firstName + ' ' + chatMember.lastName
                                    : chat.name}
                            </div>
                            {
                                lastMessage.text ?
                                    <div className='single-chat-user-message-date'>
                                        {Moment(lastMessage.sendAt).local().format('MMMM Do')}
                                    </div>
                                    :
                                    <div></div>
                            }
                        </div>
                        {
                            lastMessage.text ?
                                lastMessage.authorId == currentUserId ?
                                    <div className='single-chat-last-message-group'>
                                        <ImgAutoLoad id={currentUserChatMember.avatarId} className='single-chat-last-message-img' />
                                        <div className='single-chat-last-message-owner'>
                                            {lastMessage.text}
                                        </div>
                                        {
                                            unreadMessagesCount != 0 ?
                                                <div className='single-chat-unread-messages-count'>
                                                    {unreadMessagesCount}
                                                </div>
                                                :
                                                null
                                        }
                                    </div>
                                    :
                                    <div className='single-chat-last-message-group'>
                                        <div className='single-chat-last-message'>
                                            {lastMessage.text}
                                        </div>
                                        {
                                            unreadMessagesCount != 0 ?
                                                <div className='single-chat-unread-messages-count'>
                                                    {unreadMessagesCount}
                                                </div>
                                                :
                                                null
                                        }
                                    </div>
                                :
                                <div>
                                </div>
                        }
                    </div>
                </div>

            </div>
        </Link>
    );
}

export default SingleChatRow;