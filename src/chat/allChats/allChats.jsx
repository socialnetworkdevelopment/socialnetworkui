import React from 'react';
import ChatList from './chatList';
import Header from '../../components/header/header';
import './allChats.css';
import '../../css/main.css';
import withSessionInfo from '../../sessionInfoWrapper';

import NavigationComponent from "../../components/navigation/navigation";

const AllChats = function () {

  return (
    <div className='common-background-content-page'>
      <Header />
      <div className='common-content-page-container'>
        <NavigationComponent />
        <ChatList />
      </div>
    </div>
  );
}


export default withSessionInfo(AllChats);




