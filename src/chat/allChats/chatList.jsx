import React, { useEffect, useState } from 'react';
import SingleChatRow from './singleChatRow';
import withSessionInfo from '../../sessionInfoWrapper';
import { Link } from 'react-router-dom';
import AppConfig from '../../AppConfig';
import axios from 'axios';

function ChatList({currentUserId }) {

    const [chats, setChats] = useState([]);

    const sortingChats = (chats) => {
        return chats.sort((chat1, chat2) => {
            if (!chat1.lastMessage && !chat2.lastMessage || (chat1.lastMessage && chat2.lastMessage && chat1.lastMessage.sendAt == chat2.lastMessage.sendAt)) {
                if (chat1.id > chat2.id)
                    return -1;
                else if (chat1.id < chat2.id)
                    return 1;
            }
            else if (!chat1.lastMessage && chat2.lastMessage || (chat1.lastMessage && chat2.lastMessage && chat1.lastMessage.sendAt < chat2.lastMessage.sendAt))
                return 1;
            else
                return -1;
        })
    }

    useEffect(() => {
        axios.get(`${AppConfig.chatServerUrl}/api/chats?userId=${currentUserId}`)
            .then((response) => {
                const promises = response.data.map(chat => {
                    return axios.get(`${AppConfig.chatServerUrl}/api/messages/search?chatId=${chat.id}&takeCount=1&descending=true`)
                        .then((messageResponse) => {
                            chat.lastMessage = messageResponse.data[0];
                        })
                        .catch((error) => {
                            console.error('Error fetching chats:', error);
                        });
                });

                Promise.all(promises).then(() => {
                    setChats(sortingChats(response.data));
                });
            })
            .catch((error) => {
                console.error('Error fetching chats:', error);
            });


    }, [])

    return (
        <div className='chats-main-panel'>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ width: '90%' }}>
                    <h2 style={{ textAlign: 'center' }}>Chat List</h2>
                </div>
                <div style={{ display: 'grid', alignContent: 'center' }}>
                    <Link to={`/createGroupChat`}>
                        <img src={require('../../images/chat/createGroupChat.png')} width={40} height={40}></img>
                    </Link>
                </div>
            </div>
            <div className='chats-container'>
                {chats.map((chat) => (
                    <SingleChatRow key={chat.id} chat={chat} currentUserId={currentUserId} setChats={setChats} sortingChats={sortingChats}></SingleChatRow>
                ))}
            </div>
        </div>
    );
}


export default withSessionInfo(ChatList);