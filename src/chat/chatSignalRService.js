import * as signalR from "@microsoft/signalr";
import AppConfig from '../AppConfig';
import { store } from '../store';

let hubConnection;

export const chatSignalRService = {
  startConnection: async function () {

    const accessTokenFactory = async () => {
      return `${store.getState().session.token}`;
    };

    if (!hubConnection) {
      hubConnection = new signalR.HubConnectionBuilder()
        .withUrl(`${AppConfig.chatServerUrl}/chatHub`, {
          skipNegotiation: true,
          transport: signalR.HttpTransportType.WebSockets,
          accessTokenFactory: accessTokenFactory
        })
        .withAutomaticReconnect()
        .build();

      try {
        await hubConnection.start();
        console.log("SignalR Connected.");
      } catch (err) {
        console.log(err);
      }
    }
    else
      console.log('hubConneciton is null');

    if (hubConnection.state === signalR.HubConnectionState.Disconnected) {

      hubConnection.accessTokenFactory = accessTokenFactory;

      try {
        await hubConnection.start();
        console.log("SignalR Connected.");
      } catch (err) {
        console.log(err);
      }
    }
  },

  stopConnection: function () {
    hubConnection.stop();
    console.log("SignalR Disconnected.");
  },

  onReceiveMessage: function (callback) {
    hubConnection.on("ReceiveMessage", callback);
  },

  offReceiveMessage: function(callback) {
    hubConnection.off("ReceiveMessage", callback);
  },

  onReadMessage: function (callback) {
    hubConnection.on("ReadMessage", callback);
  },

  offReadMessage: function(callback) {
    hubConnection.off("ReadMessage", callback);
  },
};