import React, { useState } from 'react';

function InputMessageRow({ onSend }) {
    const [message, setMessage] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        if (message.trim() !== '') {
            onSend(message.trim());
            setMessage('');
        }
    };

    const handleChange = (event) => {
        setMessage(event.target.value);
    };

    return (
        <form onSubmit={handleSubmit}>
            <div className='single-chat-message-input-row'>

            <input className='single-chat-message-input'
                type="text"
                value={message}
                onChange={handleChange}
                placeholder="Write message..."
            />
            </div>
        </form>
    );
}

export default InputMessageRow;