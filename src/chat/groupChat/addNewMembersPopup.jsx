import React, { useState, useEffect } from 'react';
import withSessionInfo from '../../sessionInfoWrapper';
import FriendsListForAdded from './components/friendsListForAdded';
import axios from 'axios';
import AppConfig from '../../AppConfig';

function AddNewMembersPopup({ currentUserId, chat, visible, setVisible, setVisibleParent, chatMembers, setChatMembers, friendsToAdd, setFriendsToAdd }) {

    const [selectedFriends, setSelectedFriends] = useState([]);

    const chatMembersIds = chat ? chatMembers.map(chatMember => chatMember.userId) : [];

    const onBackClick = () => {
        setVisible(false);
        setVisibleParent(true);
    }

    const onAddMebmers = () => {
        if (selectedFriends.length < 1) {
            alert('You should choose at leat 1 friend to add to group chat.')
            return;
        }

        axios.put(`${AppConfig.chatServerUrl}/api/chats/group/${chat.id}/addUser`, selectedFriends.map(selectedFriend => selectedFriend.id))
            .then(response => {
                var addedUserIds = response.data.map(addedUser => addedUser.userId);
                console.log(response.data)
                setChatMembers(prev => prev.concat(response.data))
                setVisible(false);
                setFriendsToAdd(prev => prev.filter(friendToAdd => !addedUserIds.includes(friendToAdd.id)))
                setSelectedFriends(prev => prev.filter(friendToAdd => !addedUserIds.includes(friendToAdd.id)))
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    useEffect(() => {

        let url = AppConfig.usersUrl + `/api/users/${currentUserId}/friends`;
        axios.get(url).then((response) => {

            setFriendsToAdd(response.data.friends.filter(friend => !chatMembersIds.includes(friend.id)));
            console.log(response.data);
        })
            .catch((error) => {
                console.error('Error fetching friends:', error);
            });
    }, [])

    const rootClasses = ['edit-group-chat-modal'];
    if (visible)
        rootClasses.push('active');

    return (
        <div className={rootClasses.join(' ')} onClick={() => setVisible(false)}>
            <div className='edit-group-chat-modal-content' onClick={(e) => e.stopPropagation()}>
                <div>
                    <button onClick={() => onBackClick()} style={{
                        position: 'relative', left: '-25px', top: '-25px',
                        margin: '0', borderRadius: '15px 0', background: '#39456C'
                    }}>Back</button>
                    <div className='gorup-chat-main-panel-modal' style={{ top: '-25px', background: '' }}>
                        <div className='group-chat-info-info-group-frame' style={{ justifyContent: 'center' }}>
                            <h2 style={{ textAlign: 'center' }}>Adding new members</h2>
                        </div>

                        <FriendsListForAdded selectedFriends={selectedFriends} setSelectedFriends={setSelectedFriends} chat={chat}
                            chatMembers={chatMembers} setChatMembers={setChatMembers} friendsToAdd={friendsToAdd} />
                        <button className='group-chat-update-button' onClick={onAddMebmers}>Add</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default withSessionInfo(AddNewMembersPopup);