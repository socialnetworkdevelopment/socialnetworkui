import ImgAutoLoad from '../../components/imgAutoLoad';
import GroupChatMembersList from './components/groupChatMembersList';
import AppConfig from '../../AppConfig';
import React, { useState } from 'react';
import axios from 'axios';
import withSessionInfo from '../../sessionInfoWrapper';
import DivUploadImg from '../../components/uploaderFiles/divUploadImg';

function GroupChatInfoPopup({ chat, visible, setVisible, setVisibleAddNewMembersPopup, chatMembers, setChatMembers, setFriendsToAdd, setState, currentUserId }) {

    const rootClasses = ['edit-group-chat-modal'];
    if (visible)
        rootClasses.push('active');

    const [groupChatName, setGroupChatName] = useState(chat.name);
    const [currentUserIsAdmin, setCurrentUserIsAdmin] = useState(chatMembers.some(chatMember => chatMember.isAdmin && chatMember.userId == currentUserId));

    const newImageUploading = (id) => {
        axios.put(AppConfig.chatServerUrl + `/api/chats/${chat.id}`, { imgId: id })
            .then((res) => {
                axios.delete(AppConfig.filesUrl + `/api?id=${chat.imgId}`);
                setState(prevState => ({
                    chat: {
                        ...prevState.chat,
                        imgId: id
                    }
                }));
            })
            .catch((error) => {
                console.log(error)
                axios.delete(AppConfig.filesUrl + `/api?id=${id}`);
            })
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        const newChatName = groupChatName.trim();
        if (newChatName !== '') {
            axios.put(AppConfig.chatServerUrl + `/api/chats/${chat.id}`, { name: newChatName })
                .then((res) => {
                    setState(prevState => ({
                        chat: {
                            ...prevState.chat,
                            name: newChatName
                        }
                    }));
                })
                .catch((error) => {
                    console.log(error)
                })
        }
        else
            alert('The chat name shouldn\'t be empty.');
    };

    const handleChange = (event) => {
        setGroupChatName(event.target.value);
    };

    return (
        <div className={rootClasses.join(' ')} onClick={() => {
            setVisible(false)
            setGroupChatName(chat.name)
        }}>
            <div className='edit-group-chat-modal-content' onClick={(e) => e.stopPropagation()}>
                <form onSubmit={handleSubmit}>
                    <div className='gorup-chat-main-panel-modal' style={{ position: 'unset' }}>
                        <div className='group-chat-info-info-group-frame'>
                            {
                                currentUserIsAdmin ?
                                    <>
                                        <DivUploadImg result={newImageUploading}>
                                            <ImgAutoLoad id={chat.imgId} className={'single-chat-user-img group-chat-button-change-image'}
                                                style={{ width: '100px', height: '100px' }} />
                                        </DivUploadImg>
                                        <input style={{
                                            textAlign: 'left', paddingLeft: '15px', width: 'auto',
                                            fontFamily: 'Inter',
                                            fontStyle: 'normal',
                                            fontWeight: '400',
                                            fontSize: '20px',
                                            background: '#2F3136'
                                        }} value={groupChatName} type="text" onChange={handleChange}></input>
                                        <button style={{background: '#2F3136'}}>
                                            <img src={require('../../images/chat/saveIcon.png')} style={{width: '30px', height: '30px'}} ></img>
                                        </button>
                                    </>
                                    :
                                    <>
                                        <ImgAutoLoad id={chat.imgId} className={'single-chat-user-img'} style={{ width: '100px', height: '100px' }} />
                                        <h2 style={{ textAlign: 'center', paddingLeft: '15px' }}>{groupChatName}</h2>
                                    </>
                            }
                        </div>
                        <GroupChatMembersList chat={chat} setVisible={setVisible} setVisibleAddNewMembersPopup={setVisibleAddNewMembersPopup}
                            chatMembers={chatMembers} setChatMembers={setChatMembers} setFriendsToAdd={setFriendsToAdd}
                            currentUserIsAdmin={currentUserIsAdmin} setCurrentUserIsAdmin={setCurrentUserIsAdmin} />
                    </div>
                </form>
            </div>

        </div>
    );
}

export default withSessionInfo(GroupChatInfoPopup);