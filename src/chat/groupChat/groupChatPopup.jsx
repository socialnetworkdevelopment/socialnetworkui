import GroupChatInfoPopup from './groupChatInfoPopup';
import AddNewMembersPopup from './addNewMembersPopup';
import React, { useState, useEffect } from 'react';

function GroupChatPopup({ chat, visibleGroupChatInfoPopup, setVisibleGroupChatInfoPopup, visibleAddNewMembersPopup, setVisibleAddNewMembersPopup }) {

    const [chatMembers, setChatMembers] = useState(chat.chatMembers);
    

    return (
        <>
            <GroupChatInfoPopup chat={chat} visible={visibleGroupChatInfoPopup}
                setVisible={setVisibleGroupChatInfoPopup} setVisibleAddNewMembersPopup={setVisibleAddNewMembersPopup}
                chatMembers={chatMembers} setChatMembers={setChatMembers} />
            <AddNewMembersPopup chat={chat} setVisibleParent={setVisibleGroupChatInfoPopup}
                visible={visibleAddNewMembersPopup} setVisible={setVisibleAddNewMembersPopup}
                chatMembers={chatMembers} setChatMembers={setChatMembers}></AddNewMembersPopup>
        </>
    );
}

export default GroupChatPopup;