import withSessionInfo from '../../../sessionInfoWrapper';
import '../groupChat.css';
import '../../../css/main.css';
import SelectedFriend from './selectedFriend';

function SelectedFriends({ selectedFriends, onDeleteFriend }) {

    return (
        <div>
            {
                selectedFriends.length > 0 ?
                    <div>
                        <div className='selected-friends-row'>
                            {selectedFriends.map((friendUser) => (
                                <SelectedFriend friendUser={friendUser} onDeleteFriend={onDeleteFriend} key={friendUser.id}></SelectedFriend>
                            ))}
                        </div>
                        <div className='group-chat-friends-horizontal-line' style={{ marginBottom: '5px' }} />
                    </div>
                    :
                    null
            }
        </div>
    );
}


export default withSessionInfo(SelectedFriends);