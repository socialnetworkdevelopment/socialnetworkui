import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import withSessionInfo from '../../../sessionInfoWrapper';
import AppConfig from "../../../AppConfig";
import '../groupChat.css';
import '../../../css/main.css';
import axios from 'axios';
import GroupChatMember from './groupChatMember';

function GroupChatMembersList({ chat, setVisible, setVisibleAddNewMembersPopup, chatMembers, setChatMembers, setFriendsToAdd, currentUserId,
    currentUserIsAdmin, setCurrentUserIsAdmin }) {

    const omRemove = (chatMemberUser) => {
        axios.delete(`${AppConfig.chatServerUrl}/api/chat_members?chatId=${chat.id}&userId=${chatMemberUser.userId}`)
            .then(() => {
                setChatMembers(prev => prev.filter(chatMember => chatMember.userId != chatMemberUser.userId));
                setFriendsToAdd(prev => [...prev, { id: chatMemberUser.userId, firstName: chatMemberUser.firstName, lastName: chatMemberUser.lastName }])
            })
            .catch((error) => {
                console.error('Error fetching chats:', error);
            });
    };

    const onSetAsAdmin = (chatMemberUser) => {
        console.log(chatMemberUser.userId)
        axios.post(`${AppConfig.chatServerUrl}/api/chats/${chat.id}/addGroupAdmin`, chatMemberUser.userId,  { 'headers': {
            'Content-Type': 'application/json; charset=utf-8'
          }})
            .then(() => {
                setChatMembers(prev => {
                    var newChatMembers = prev.map(chatMember => {
                        if (chatMember.userId == chatMemberUser.userId) {
                            chatMember.isAdmin = true;
                        }

                        return chatMember;
                    })
                    return newChatMembers;
                });
            })
            .catch((error) => {
                console.error('Error fetching chats:', error);
            });
    };

    const clickOpenAddMembersPopupHandler = () => {
        setVisible(false);
        setVisibleAddNewMembersPopup(true);
    }

    return (
        <>
            <div className='group-chat-friends-horizontal-line' style={{ marginBottom: '5px' }} />
            <div className='friends-container'>
                <Link onClick={() => clickOpenAddMembersPopupHandler()}>
                    <div className='single-friend-row'>
                        <div className='group-chat-row-group-frame'>
                            <div className='group-chat-friend-row-user-image-container'>
                                <img className='group-chat-friend-row-user-image' src={require('../../../images/chat/addCross.png')}></img>
                            </div>
                            <div className='group-chat-friend-row-user-info-group'>
                                <div className='group-chat-friend-row-user-name'>
                                    <div className='group-chat-friend-row-user-name-text' style={{ color: '#919191' }}>
                                        Add new chat member
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='group-chat-friends-horizontal-line' />
                    </div>
                </Link>
                {chatMembers.map((chatMember) => (
                    <GroupChatMember key={chatMember.id} chatMemberUser={chatMember} chat={chat} onRemoveHandler={omRemove}
                        onSetAsAdminHandler={onSetAsAdmin} currentUserIsAdmin={currentUserIsAdmin}></GroupChatMember>
                ))}
            </div>
        </>
    );
}


export default withSessionInfo(GroupChatMembersList);