import React, { useState, useEffect } from 'react';
import withSessionInfo from '../../../sessionInfoWrapper';
import { Link } from 'react-router-dom';
import AppConfig from "../../../AppConfig";
import '../groupChat.css';
import '../../../css/main.css';
import axios from 'axios';
import FriendRowForAdd from './friendRowForAdd';
import SelectedFriends from './selectedFriends';

function FriendsListForAdded({ currentUserId, selectedFriends, setSelectedFriends, chat, chatMembers, friendsToAdd }) {


    const onSelectFriend = (wasAdd, friend) => {
        if (wasAdd) {
            setSelectedFriends(prevFriends => [...prevFriends, friend]);
        } else {
            setSelectedFriends(prevFriends => prevFriends.filter(f => f !== friend));
        }
    }

    const onDeleteFriend = (friend) => {
        onSelectFriend(false, friend);
    }

    return (
        <>
            <div className='group-chat-friends-horizontal-line' style={{ marginBottom: '5px' }} />
            <SelectedFriends selectedFriends={selectedFriends} onDeleteFriend={onDeleteFriend}></SelectedFriends>
            <div className='friends-container'>
                {
                    friendsToAdd.length == 0
                        ?
                        <h3>Haven't got friends...</h3>
                        :
                        friendsToAdd.map((friend) => (
                            <FriendRowForAdd key={friend.id} friendUser={friend} onSelectFriend={onSelectFriend} selectedFriends={selectedFriends}></FriendRowForAdd>
                        ))}
            </div>
        </>
    );
}


export default withSessionInfo(FriendsListForAdded);