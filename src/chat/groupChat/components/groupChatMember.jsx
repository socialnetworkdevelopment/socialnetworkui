import React, { } from 'react';
import { Link } from 'react-router-dom';
import '../groupChat.css';
import '../../../css/main.css';
import withSessionInfo from '../../../sessionInfoWrapper';
import ImgAutoLoad from '../../../components/imgAutoLoad';

function GroupChatMember({ chat, chatMemberUser, currentUserId, onRemoveHandler, currentUserIsAdmin, onSetAsAdminHandler }) {  

    const onRemoveClick = () => {
        onRemoveHandler(chatMemberUser);
    }

    const onSetAsAdminClick = () => {
        onSetAsAdminHandler(chatMemberUser);
    }
    
    return (
        <div className='single-friend-row'>
            <div className='group-chat-row-group-frame'>
                <div className='group-chat-friend-row-user-image-container'>
                    <ImgAutoLoad id={chatMemberUser.avatarId} className={'group-chat-friend-row-user-image'} />
                </div>
                <div className='group-chat-friend-row-user-info-group'>
                    <div className='group-chat-friend-row-user-name'>
                        <Link to={`/profile/${chatMemberUser.id}`}>
                            <div className='group-chat-friend-row-user-name-text'>
                                {chatMemberUser.firstName + ' ' + chatMemberUser.lastName}
                            </div>
                        </Link>
                    </div>
                </div>
                {
                    currentUserIsAdmin && !chatMemberUser.isAdmin ?
                    <button className='group-chat-set-admin-button' onClick={onSetAsAdminClick}>set admin</button>
                    :
                    null
                }
                {
                    chatMemberUser.isAdmin ?
                    <div className='group-chat-user-admin-info'>admin</div>
                    :
                    null
                }
                {
                    chatMemberUser.userId == currentUserId || currentUserIsAdmin ?
                    <img src={require('../../../images/chat/cross.png')} onClick={onRemoveClick}></img>
                    :
                    null
                }
            </div>
            <div className='group-chat-friends-horizontal-line' />
        </div>

    );
}


export default withSessionInfo(GroupChatMember);