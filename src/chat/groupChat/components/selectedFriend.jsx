import React from 'react';
import withSessionInfo from '../../../sessionInfoWrapper';
import '../groupChat.css';
import '../../../css/main.css';

function SelectedFriend({ currentUserId, friendUser, onDeleteFriend }) {

    const handleClick = (event) => {
        onDeleteFriend(friendUser);
    };

    return (
        <div className='selected-friend-row' key={friendUser.Id}>
            <div className='selected-friend-row-user-name-text'>
                {friendUser.firstName + ' ' + friendUser.lastName}
            </div>
            <img src={require('../../../images/chat/cross.png')} onClick={handleClick}></img>
        </div>
    );
}


export default withSessionInfo(SelectedFriend);