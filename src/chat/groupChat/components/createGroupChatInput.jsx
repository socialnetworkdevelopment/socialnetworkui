import React, { useState } from 'react';

function CreateGroupChatInput({ onCreate }) {

    const [chatName, setChatName] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        if (chatName.trim() !== '') {
            onCreate(chatName.trim());
            setChatName('');
        }
        else
            alert('The chat name shouldn\'t be empty.');
    };

    const handleChange = (event) => {
        setChatName(event.target.value);
    };

    return (
        <form onSubmit={handleSubmit}>
            <div className='group-chat-create-input-row'>

            <input className='group-chat-create-input'
                type="text"
                onChange={handleChange}
                placeholder="Write chat group name..."
            />
            <button className='create-group-chat-button'>Create chat</button>
            </div>
        </form>
    );
}

export default CreateGroupChatInput;