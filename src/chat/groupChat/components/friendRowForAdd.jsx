import React, {  } from 'react';
import withSessionInfo from '../../../sessionInfoWrapper';
import '../groupChat.css';
import '../../../css/main.css';
import ImgAutoLoad from '../../../components/imgAutoLoad';

function FriendRowForAdd({ currentUserId, friendUser, onSelectFriend, selectedFriends }) {

    const handleClick = (event) => {
        onSelectFriend(event.target.checked, friendUser);
    };
    
    return (
        <div className='single-friend-row'>
            <div className='group-chat-row-group-frame'>
                <div className='group-chat-friend-row-user-image-container'>
                    <ImgAutoLoad id={friendUser.avatarId} className={'group-chat-friend-row-user-image'} />
                </div>
                <div className='group-chat-friend-row-user-info-group'>
                    <div className='group-chat-friend-row-user-name'>
                        <div className='group-chat-friend-row-user-name-text'>
                            {friendUser.firstName + ' ' + friendUser.lastName}
                        </div>
                    </div>
                </div>
                <div>
                    <input type='checkbox' checked={selectedFriends.filter(friend => friend === friendUser).length > 0} id={friendUser.id} 
                    className='group-chat-friend-checkbox' onChange={handleClick}/>
                </div>
            </div>
            <div className='group-chat-friends-horizontal-line'/>
        </div>
    );
}


export default withSessionInfo(FriendRowForAdd);