import React, { useEffect, useState } from 'react';
import Header from '../../components/header/header';
import '../allChats/allChats.css';
import '../../css/main.css';
import axios from 'axios';
import withSessionInfo from '../../sessionInfoWrapper';
import AppConfig from '../../AppConfig';
import NavigationComponent from "../../components/navigation/navigation";
import FriendsListForAdded from './components/friendsListForAdded';
import CreateGroupChatInput from './components/createGroupChatInput';
import { useNavigate } from 'react-router-dom';

const CreateGroupChat = function ({ currentUserId }) {

    const [selectedFriends, setSelectedFriends] = useState([]);

    const navigate = useNavigate()

    const [friendsToAdd, setFriendsToAdd] = useState([]);

    const onCreateChatGroup = (chatName) => {
        if (selectedFriends.length < 2) {
            alert('You should choose more than 1 friend to create group chat.')
            return;
        }

        axios.post(`${AppConfig.chatServerUrl}/api/chats/group`, {
            Name: chatName,
            CreatedBy: currentUserId,
            ChatMembers: selectedFriends.map(selectedFriend => selectedFriend.id)
        })
            .then(response => {
                console.log('Success:', response);
                navigate('/allChats');
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    useEffect(() => {

        let url = AppConfig.usersUrl + `/api/users/${currentUserId}/friends`;
        axios.get(url).then((response) => {
            setFriendsToAdd(response.data.friends);
        })
            .catch((error) => {
                console.error('Error fetching friends:', error);
            });
    }, [])

    return (
        <div className='common-background-content-page'>
            <Header />
            <div className='common-content-page-container'>
                <NavigationComponent />
                <div className='gorup-chat-main-panel'>
                    <h2 style={{ textAlign: 'center' }}>Creating group chat</h2>
                    <FriendsListForAdded selectedFriends={selectedFriends} setSelectedFriends={setSelectedFriends} friendsToAdd={friendsToAdd} />
                    <CreateGroupChatInput onCreate={onCreateChatGroup} />
                </div>
            </div>
        </div >
    );
}


export default withSessionInfo(CreateGroupChat);




