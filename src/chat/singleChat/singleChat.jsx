import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Header from '../../components/header/header';
import '../../css/main.css';
import './singleChat.css';

import { chatSignalRService } from "../chatSignalRService.js";
import InputMessageRow from '../commonComponents/inputMessageRow';
import Messages from './components/privateChat/messages';
import GroupMessages from './components/groupChat/messages';
import UserInfo from './components/privateChat/userInfo';
import AppConfig from '../../AppConfig';
import withSessionInfo from '../../sessionInfoWrapper';
import NavigationComponent from "../../components/navigation/navigation";
import GroupChatInfo from './components/groupChat/groupChatInfo'

import axios from 'axios';
import GroupChatInfoPopup from '../groupChat/groupChatInfoPopup';
import AddNewMembersPopup from '../groupChat/addNewMembersPopup';
import Moment from 'moment';

const SingleChat = function ({ currentUserId }) {
    const { id } = useParams();
    const [state, setState] = useState({
        id: id,
        chat: null,
        loading: true
    });

    const [messages, setMessages] = useState([]);

    const [visibleGroupChatInfoPopup, setVisibleGroupChatInfoPopup] = useState(false);
    const [visibleAddNewMembersPopup, setVisibleAddNewMembersPopup] = useState(false);
    const [friendsToAdd, setFriendsToAdd] = useState([]);

    const [chatMembers, setChatMembers] = useState();

    const onReceiveMessage = (user, chatId, messageId, messageText, sendAt) => {
        if (id != chatId)
            return;

        if (user.id != currentUserId) {
            axios.put(`${AppConfig.chatServerUrl}/api/messages/read`, {
                messageId,
                chatId
            })
                .catch((error) => {
                    console.error('Error fetching chats:', error);
                });

            var message = { text: messageText, id: messageId, authorId: user.id, isRead: true, sendAt: sendAt };
            setMessages(prevMessages => [message, ...prevMessages]);
        }
        else {
            setMessages(prevMessages => {
                var currentMessage = prevMessages.filter(prevMessage => prevMessage.id == messageId)[0];
                if (!currentMessage) {
                    var message = { text: messageText, id: messageId, authorId: user.id, isRead: false, sendAt: sendAt };
                    return [message, ...prevMessages];
                }
                else
                    return prevMessages;
            });
        }
    }

    const onReadMessage = (messageId, chatId) => {
        console.log('receive message')
        if (id != chatId)
            return;

        setMessages(prevMessages => {
            prevMessages.forEach(message => {
                if (message.id == messageId)
                {
                    console.log(message.id)
                    message.isRead = true;
                }

            });
            return [...prevMessages];
        });
    }

    const onSendMessage = (messageText) => {
        var newMessageId = 0;
        axios.post(`${AppConfig.chatServerUrl}/api/messages`, {
            Text: messageText,
            AuthorId: currentUserId,
            ChatId: state.chat.id
        })
            .then(response => {
                console.log('Success:', response);
                console.log(Moment().local().format('HH:mm'));
                newMessageId = response.data;
                const message = { text: messageText, id: newMessageId, authorId: currentUserId, sendAt: Moment().local().format() };
                setMessages(prevMessage => [message, ...prevMessage]);
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    useEffect(() => {
        async function fetchChat() {
            await axios.get(`${AppConfig.chatServerUrl}/api/chats/${state.id}`)
                .then(async (response) => {
                    if (response.data.type == 1) {
                        try {
                            const groupAdminsResponse = await axios.get(`${AppConfig.chatServerUrl}/api/chats/${state.id}/groupAdmins`);
                            response.data.chatMembers = response.data.chatMembers.map(chatMember => {
                                if (groupAdminsResponse.data.filter(chatAdmin => chatAdmin.userId == chatMember.userId).length > 0) {
                                    chatMember.isAdmin = true;
                                }
                                return chatMember;
                            });
                            setChatMembers(response.data.chatMembers);
                        } catch (error) {
                            console.error('Error fetching group admins:', error);
                        }
                    }

                    setState({
                        ...state,
                        chat: response.data,
                        loading: false,
                        currentUserId: currentUserId,
                    });
                    setChatMembers(response.data.chatMembers)
                })
                .catch((error) => {
                    console.error('Error fetching chat:', error);
                });
        }

        fetchChat();

        chatSignalRService.onReceiveMessage(onReceiveMessage);
        chatSignalRService.onReadMessage(onReadMessage);

        return () => {
            chatSignalRService.offReceiveMessage(onReceiveMessage);
            chatSignalRService.offReadMessage(onReadMessage);
        }
    }, [])

    return (
        <div className='common-background-content-page'>
            <Header />
            <div className='common-content-page-container'>
                <NavigationComponent />
                {
                    state.loading ?
                        <div className='single-chat-main-container'></div>
                        :
                        <div className='single-chat-main-container'>
                            {
                                state.chat.type == 1 ?
                                    <>
                                        <GroupChatInfoPopup chat={state.chat} visible={visibleGroupChatInfoPopup}
                                            setVisible={setVisibleGroupChatInfoPopup} setVisibleAddNewMembersPopup={setVisibleAddNewMembersPopup}
                                            chatMembers={chatMembers} setChatMembers={setChatMembers} setFriendsToAdd={setFriendsToAdd}
                                            setState={setState} />
                                        <AddNewMembersPopup chat={state.chat} setVisibleParent={setVisibleGroupChatInfoPopup}
                                            visible={visibleAddNewMembersPopup} setVisible={setVisibleAddNewMembersPopup}
                                            chatMembers={chatMembers} setChatMembers={setChatMembers} friendsToAdd={friendsToAdd}
                                            setFriendsToAdd={setFriendsToAdd}></AddNewMembersPopup>
                                    </>
                                    :
                                    <></>
                            }
                            {
                                state.chat.type == 0 ?
                                    <UserInfo chat={state.chat} currentUserId={currentUserId} />
                                    :
                                    <GroupChatInfo chat={state.chat} setVisibleInfoPopup={setVisibleGroupChatInfoPopup} />
                            }
                            <div className='single-chat-horizontal-line'></div>
                            {
                                state.chat.type == 0 ?
                                    <Messages chat={state.chat} messages={messages} setMessages={setMessages} currentUserId={currentUserId} />
                                    :
                                    <GroupMessages chat={state.chat} messages={messages} setMessages={setMessages} currentUserId={currentUserId} />
                            }
                            <InputMessageRow onSend={onSendMessage} />
                        </div>
                }
            </div>
        </div>
    )
}


export default withSessionInfo(SingleChat);