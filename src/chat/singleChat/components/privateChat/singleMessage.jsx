import Moment from 'moment';

function SingleMessage({ message, currentUserId, dateInfo }) {

    return (
        <>

            {
                dateInfo.shouldRender &&
                !dateInfo.showBelow && (
                    <div className='single-chat-message-date-delimeter'>
                        {dateInfo.date}
                    </div>
                )
            }
            {
                message.authorId == currentUserId ?


                    <div className='single-chat-message-row-owner'>
                        <div className='single-chat-message-text'>
                            {message.text}
                        </div>
                        <div className='single-chat-message-time'>
                            {Moment(message.sendAt).local().format('HH:mm')}
                        </div>
                        <div className='single-chat-message-check-mark'>
                            {message.isRead ?
                                <img src={require('../../../../images/chat/checkMarkBlue.png')} width={20} height={20}></img>
                                :
                                <img src={require('../../../../images/chat/checkMarkGrey.png')} width={20} height={20}></img>
                            }
                        </div>
                    </div>
                    :


                    <div className='single-chat-message-row-companion'>
                        <div className='single-chat-message-text'>
                            {message.text}
                        </div>
                        <div className='single-chat-message-time'>
                            {Moment(message.sendAt).local().format('HH:mm')}
                        </div>
                    </div>
            }
            {
                dateInfo.shouldRender &&
                dateInfo.showBelow && (
                    <div className='single-chat-message-date-delimeter'>
                        {dateInfo.date}
                    </div>
                )
            }
        </>
    )
}

export default SingleMessage;