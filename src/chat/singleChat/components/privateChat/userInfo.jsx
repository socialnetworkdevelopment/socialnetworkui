import { Link } from 'react-router-dom';
import ImgAutoLoad from '../../../../components/imgAutoLoad';

function UserInfo({ chat, currentUserId }) {

    const chatMember = chat.chatMembers.filter(chatMember => chatMember.userId != currentUserId)[0];

    return (
        <div className='single-chat-info-row'>
            <Link to={`/allChats`}>
                <div className='single-chat-all-chat-link'>
                    All chats
                </div>
            </Link>
            <div className='single-chat-user-info'>
                <ImgAutoLoad id={chatMember.avatarId} className={'single-chat-user-img'} />
                <Link to={`/profile/${chatMember.userId}`}>
                    <div className='single-chat-user-name'>
                        {`${chatMember.firstName} ${chatMember.lastName}`}
                    </div>
                </Link>
            </div>

        </div>
    );
}

export default UserInfo;