import Moment from 'moment';
import ImgAutoLoad from '../../../../components/imgAutoLoad';

function SingleMessage({ message, currentUserId, shouldShowAuthor, chatMembers, dateInfo }) {
    let authorInfo;
    if (shouldShowAuthor) {
        authorInfo = chatMembers.filter(chatMemberInfo => chatMemberInfo.userId == message.authorId)[0];
    }

    return (
        <>
            {
                dateInfo.shouldRender &&
                !dateInfo.showBelow && (
                    <div className='single-chat-message-date-delimeter'>
                        {dateInfo.date}
                    </div>
                )
            }
            {
                message.authorId == currentUserId ?

                    <div className='single-chat-message-row-owner'>
                        <div className='single-chat-message-text'>
                            {message.text}
                        </div>
                        <div className='single-chat-message-time'>
                            {Moment(message.sendAt).local().format('HH:mm')}
                        </div>
                        <div className='single-chat-message-check-mark'>
                            {message.isRead ?
                                <img src={require('../../../../images/chat/checkMarkBlue.png')} width={20} height={20}></img>
                                :
                                <img src={require('../../../../images/chat/checkMarkGrey.png')} width={20} height={20}></img>
                            }
                        </div>
                    </div>

                    :

                    <div className='single-chat-message-row-companion-group-container'>
                        {
                            shouldShowAuthor ?
                                <ImgAutoLoad id={authorInfo.avatarId} style={{ width: '35px', height: '35px', borderRadius: '50%', marginRight: '10px', objectFit: 'cover' }} />
                                :
                                <div style={{ width: '45px' }} />
                        }

                        <div className='single-chat-message-row-companion-group'>
                            <div style={{ display: 'flex', flexDirection: 'column' }}>
                                {
                                    shouldShowAuthor ?
                                        <div className='single-chat-group-message-user-name'>{`${authorInfo.firstName} ${authorInfo.lastName}`}</div>
                                        :
                                        null
                                }
                                <div style={{ display: 'flex', flexDirection: 'row' }}>
                                    <div className='single-chat-message-text'>
                                        {message.text}
                                    </div>
                                    <div className='single-chat-message-time'>
                                        {Moment(message.sendAt).local().format('HH:mm')}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            }
            {
                dateInfo.shouldRender &&
                dateInfo.showBelow && (
                    <div className='single-chat-message-date-delimeter'>
                        {dateInfo.date}
                    </div>
                )
            }
        </>
    );
}

export default SingleMessage;