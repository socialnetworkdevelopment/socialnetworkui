import { Link } from 'react-router-dom';
import withSessionInfo from '../../../../sessionInfoWrapper';
import ImgAutoLoad from '../../../../components/imgAutoLoad';

function GroupChatInfo({ chat, setVisibleInfoPopup }) {
    console.log(chat)
    return (
        <div className='single-chat-info-row'>
            <Link to={`/allChats`}>
                <div className='single-chat-all-chat-link'>
                    All chats
                </div>
            </Link>
            <div className='single-chat-user-info'>
                    <ImgAutoLoad id={chat.imgId} className={'single-chat-user-img'} />
                    <div className='single-chat-user-name' onClick={() => setVisibleInfoPopup(true)}>
                        {`${chat.name}`}
                    </div>
            </div>

        </div>
    );
}

export default withSessionInfo(GroupChatInfo);