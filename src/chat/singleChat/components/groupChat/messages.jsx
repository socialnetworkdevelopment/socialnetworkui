import SingleMessage from './singleMessage';
import React, { useState, useEffect } from 'react';
import Moment from 'moment';
import AppConfig from '../../../../AppConfig';
import axios from 'axios';

function Messages({ chat, messages, setMessages, currentUserId }) {

    let messagesCounter = 0;
    let prevDate = messages.length > 0 ? Moment(messages[0].sendAt).local().format('MMMM Do') : '';

    const [currentMessagePage, setCurrentMessagePage] = useState(1);
    const [fetching, setFetching] = useState(true);

    const scrollHandler = (e) => {
        if (e.target.offsetHeight + Math.abs(e.target.scrollTop) >= e.target.scrollHeight - 10)
            setFetching(true);
    }


    useEffect(() => {
        if (fetching) {
            axios.get(`${AppConfig.chatServerUrl}/api/messages?chatId=${chat.id}&pageNumber=${currentMessagePage}&pageSize=${15}`)
                .then(async (response) => {

                    response.data.forEach(message => {
                        if (message.authorId != currentUserId && !message.isRead) {
                            axios.put(`${AppConfig.chatServerUrl}/api/messages/read`, {
                                messageId: message.id,
                                chatId: chat.id
                            })
                                .then(() => {
                                    message.isRead = true;
                                })
                                .catch((error) => {
                                    console.error('Error fetching chats:', error);
                                });
                        }
                    });

                    var newMessageCollection = [...messages, ...response.data];
                    console.log(newMessageCollection)
                    setMessages(newMessageCollection);
                    setCurrentMessagePage(prev => prev + 1)
                    prevDate = messages.length > 0 ? Moment(messages[0].sendAt).local().format('MMMM Do') : '';
                })
                .catch((error) => {
                    console.error('Error fetching chats:', error);
                })
                .finally(() => {
                    setFetching(false)
                });
        }
    }, [fetching])



    useEffect(() => {
        let containers = document.getElementsByClassName('single-chat-messages-container');
        if (containers.length > 0)
            containers[0].addEventListener('scroll', scrollHandler);

        return () => {
            if (containers.length > 0)
                containers[0].removeEventListener('scroll', scrollHandler);
        }
    }, [])



    return (
        <div className='single-chat-messages-container'>
            {messages.map((message) => {
                const messageDate = Moment(message.sendAt).local().format('MMMM Do');
                const shouldRenderDateBlock = messageDate !== prevDate;
                const dateToShow = prevDate;
                let shouldShowAuthor = false;

                if (shouldRenderDateBlock) {
                    prevDate = messageDate;
                }

                messagesCounter++;

                const dateInfo = {
                    shouldRender: shouldRenderDateBlock || messagesCounter == messages.length,
                    date: dateToShow,
                    showBelow: messagesCounter == messages.length
                };

                if (messages.length == messagesCounter || messages[messagesCounter].authorId != message.authorId) {
                    shouldShowAuthor = true;
                }

                return (
                    <SingleMessage key={message.id} message={message} currentUserId={currentUserId}
                        shouldShowAuthor={shouldShowAuthor} chatMembers={chat.chatMembers}
                        dateInfo={dateInfo} />
                );
            })}
        </div>
    );
}

export default Messages;