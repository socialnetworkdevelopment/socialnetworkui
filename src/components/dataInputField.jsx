import React from 'react'

const DataInputField = ({label,placeholder,value,onChange,required,pattern,type}) => {
    const onChangeValue = (e) => onChange(e.target.value)
  return (
    <div className='common-input-container'>
        <label>{label}</label>
        <input onChange={onChangeValue} placeholder={placeholder} value={value} required={required} pattern={pattern} type={type}/>
    </div>
    )
}
export default DataInputField;