import React from 'react'
import { store } from '../store';
import axios from 'axios';
import AppConfig from '../AppConfig';

const ButtonRemoveFromFriends = ({friendId,children,successCallBack}) => {
    const removeFriend = () => {
        const userId = store.getState().session.currentUserId;
        axios.delete(AppConfig.usersUrl+`/api/relationship?id1=${userId}&id2=${friendId}`)
        .then(resp=>{
            if(resp.status == 204)
            successCallBack(friendId);
        });
    }
  return (
    <div onClick={removeFriend}>
        {children}
    </div>
  )
}

export default ButtonRemoveFromFriends;