import React from "react";
import './navigation.css'
import { Link } from 'react-router-dom';

const NavigationComponent = ()=> {
    let style = "navigation-item-container";
    let styleSelected = "navigation-item-container-selected";

    let profileStyle = window.location.pathname == "/profile" ? `${style} ${styleSelected}` : style;
    let friendsStyle = window.location.pathname == "/friends" ? `${style} ${styleSelected}` : style;
    let newsStyle = window.location.pathname == "/news" ? `${style} ${styleSelected}` : style;
    let allChatsStyle = window.location.pathname == "/allChats" ? `${style} ${styleSelected}` : style;
    let testStyle = window.location.pathname == "/test" ? `${style} ${styleSelected}` : style;
        
    return (
        <div className="navigation-main-container">
            <Link to="/profile" className={profileStyle}>
                <img className="navigation-item-icon-profile"/>My profie
            </Link>
            <Link to="/friends" className={friendsStyle}>
                <img className="navigation-item-icon-friends" />Friends
            </Link>
            <Link to="/news" className={newsStyle}>
                <img className="navigation-item-icon-news" />News
            </Link>
            <Link to="/allChats" className={allChatsStyle}>
                <img className="navigation-item-icon-messenger" />Messenger
            </Link>
        </div>
    );
}

export default NavigationComponent;