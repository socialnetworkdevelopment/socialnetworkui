import React, { useEffect, useState } from 'react';
import "./header.css";
import ImgAutoLoad from '../imgAutoLoad';
import { store } from '../../store';
import axios from 'axios';
import AppConfig from '../../AppConfig';
import AuthenticationManager from '../../login/oidc_AuthenticationManager';
import { useDispatch } from 'react-redux';

const Header = function () {
const [user,setUser] = useState({})
const dispatch = useDispatch();
    useEffect(()=>{

        axios.get(AppConfig.usersUrl + `/api/users/${store.getState().session.currentUserId}`)
        .then(res=>{
            setUser(res.data)
            console.log(res.data);
        });

    },[])
    const logOut = () => {
        AuthenticationManager().signoutRedirect();
        dispatch({ type: 'UPDATE_TOKEN', payload: '' });
    }
    return (
        <div className='header-frame'>
            <div className='header-main-panel'>
                <h1 className='header-title'>SocialBuddyChat</h1>
                <div className='header-buttons-container'>
                    <a href='/profile' className='header-user-container'>
                        <div className="header-user-img-container">
                            <ImgAutoLoad id={user.avatarId}/>
                        </div>
                        <div>{user.firstName}&nbsp;</div>
                        <div>{user.lastName}</div>
                    </a> 
                    <div className='header-button-logout' onClick={logOut}>LogOut</div>

                </div>
            </div>
        </div>
    );
}

export default Header;