import React from 'react'

const DataTextAreaField = ({label,placeholder,value,onChange,required}) => {
    const onChangeValue = (e) => onChange(e.target.value)
  return (
    <div className='common-input-container'>
        <label>{label}</label>
        <textarea onChange={onChangeValue} placeholder={placeholder} value={value} required={required}/>
    </div>
  )
}
export default DataTextAreaField;