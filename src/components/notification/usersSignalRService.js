import * as signalR from "@microsoft/signalr";
import AppConfig from '../../AppConfig';
import { store } from '../../store';

let hubConnection;

export const userSignalRService = {
  startConnection: async function () {

    const accessTokenFactory = async () => {
      return `${store.getState().session.token}`;
    };

    if (!hubConnection) {
      hubConnection = new signalR.HubConnectionBuilder()
        .withUrl(`${AppConfig.usersUrl}/userHub`, {
          skipNegotiation: true,
          transport: signalR.HttpTransportType.WebSockets,
          accessTokenFactory: accessTokenFactory
        })
        .withAutomaticReconnect()
        .build();

      try {
        await hubConnection.start();
        console.log("SignalR users Connected.");
      } catch (err) {
        console.log(err);
      }
    }
    else
      console.log('hubConneciton is null');

    if (hubConnection.state === signalR.HubConnectionState.Disconnected) {

      hubConnection.accessTokenFactory = accessTokenFactory;

      try {
        await hubConnection.start();
        console.log("SignalR users connected.");
      } catch (err) {
        console.log(err);
      }
    }
  },

  stopConnection: function () {
    hubConnection.stop();
    console.log("SignalR Disconnected.");
  },




  onReceiveRelationshipRequest: function(callback){
    hubConnection.on("ReceiveRelationshipRequest",callback);
  },
  offReceiveRelationshipRequest: (callback)=>{
    hubConnection.off("ReceiveRelationshipRequest",callback);
  },

  onAcceptRelationshipRequest: (callback)=>{
    hubConnection.on("AcceptRelationshipRequest",callback);
  },
  offAcceptRelationshipRequest: (callback)=>{
    hubConnection.off("AcceptRelationshipRequest",callback);
  },

  onRejectRelationshipRequest: (callback)=>{
    hubConnection.on("RejectRelationshipRequest",callback);
  },
  offRejectRelationshipRequest: (callback)=>{
    hubConnection.off("RejectRelationshipRequest",callback);
  },
};