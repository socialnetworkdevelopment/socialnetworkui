import React, { useState,useEffect } from 'react'
import NotificationComponent from './notificationComponent';
import { userSignalRService } from './usersSignalRService';
import { chatSignalRService } from '../../chat/chatSignalRService';
import { useLocation } from 'react-router-dom';
import { store } from '../../store';

const NotificationContainerComponent = () => {

  const[notification,setNotification] = useState([]);
  const location = useLocation();

  useEffect(()=>{
      userSignalRService.startConnection();
    },[]);

    //обновлене привязки к методам signalR, во избежание использования устаревшего контекста(старого состояния массива уведомлений)
    useEffect(()=>{
      userSignalRService.onReceiveRelationshipRequest(receiveRelationshipRequest);
      userSignalRService.onAcceptRelationshipRequest(acceptRelationshipRequest);
      chatSignalRService.onReceiveMessage(receiveMessage);
      return () => {
        userSignalRService.offReceiveRelationshipRequest(receiveRelationshipRequest);
        userSignalRService.offAcceptRelationshipRequest(acceptRelationshipRequest);
        chatSignalRService.offReceiveMessage(receiveMessage);
      }
    },[notification])

    //очистка уведомлений при переходе на дурую страницу
    useEffect(()=>{
      setNotification([])
    },[location])



  /**
   * Запрос в друзья
   */
  const receiveRelationshipRequest = (relationshipRequest)=>{
    console.log(relationshipRequest);
    addNotification(relationshipRequest.requestId,
      "Заявка в друзья",
      `${relationshipRequest.sender.firstName===null?"":relationshipRequest.sender.firstName}
       ${relationshipRequest.sender.lastName===null?"":relationshipRequest.sender.lastName}`,
       relationshipRequest.sender.avatarId,
      "хочет добавить вас в друзья",
      "/friends",
      `/profile/${relationshipRequest.sender.id}`);
  }
  /**
   * Запрос в друзья принят
   */
  const acceptRelationshipRequest = (relationshipRequest) => {
    console.log(relationshipRequest);

    addNotification(relationshipRequest.requestId,
      "Заявка принята",
      `${relationshipRequest.receiver.firstName===null?"":relationshipRequest.receiver.firstName}
       ${relationshipRequest.receiver.lastName===null?"":relationshipRequest.receiver.lastName}`,
       relationshipRequest.receiver.avatarId,
      "принял заявку в друзья",
      `/profile/${relationshipRequest.receiver.id}`,
      `/profile/${relationshipRequest.receiver.id}`);
  }
  /**
   * Полученно новое сообщение
   */
  const receiveMessage = (author, chatId, messageId, message, sendAt) => {
    if(author.id != store.getState().session.currentUserId && location.pathname.split("/")[2] != chatId)
      addNotification(messageId,
        "Новое сообщение",
        `${author.firstName} ${author.lastName}`,
        author.avatarId,
        message,
        `/chat/${chatId}`,
        `/profile/${author.id}`);
  }
  /**
   * Добавить уведомление для отображения
   */
  const addNotification=(id,header,name,avatarId,text,href,hrefUser)=>{
    setNotification([...notification,{id:id,header:header,name:name,avatarId:avatarId,text:text,href:href,hrefUser:hrefUser}]);
  }
  /**
   * Закрыть/удалить уведомление
   */
  const removeNotification=(id)=>{
    setTimeout(
      ()=>setNotification(notification.filter(item=>item.id != id))
      ,300);
  }

  return (
    <div>
        {
          notification.map((item)=>{
            return <NotificationComponent 
            key={item.id}
            data={item}
            // id={item.id} 
            // header={item.header} 
            // name={item.name} 
            // text={item.text} 
            // hrefUser={item.href}
            // href={item.href}
            // hrefUser={item.hrefUser}
            removeNotification={removeNotification}/>
            })
        }
    </div>
  )
}

export default NotificationContainerComponent;
