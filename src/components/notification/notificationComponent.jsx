import React, { useEffect, useState } from 'react'
import "./notification.css"
import ImgAutoLoad from '../imgAutoLoad'

const NotificationComponent = ({data,removeNotification}) => {
    const [state,setState] = useState({style:"notification-main-container notification-main-container-show"})
    useEffect(()=>{
        setTimeout(()=>{
            setState({...state,style:"notification-main-container"})
        },1000)
    },[])

    const close = (e)=>{
        e.stopPropagation()
        setState({...state,style:"notification-main-container notification-main-container-hidden"})
        removeNotification(data.id);
    }
  return (
    <div onClick={()=>window.location.replace(data.href)} className={state.style}>
        <div className='notification-header-container'>
            <div>{data.header}</div>
            <div className='notification-header-close-btn' onClick={close}>x</div>
        </div>
        <div className='notification-content-container'>
            <a href={data.hrefUser} className='notification-content-img'>
                <ImgAutoLoad id={data.avatarId}/>
            </a>
            <div className='notification-content-text'>
                <a href={data.hrefUser}><span>{data.name}</span></a> {data.text}
            </div>
        </div>
    </div>
  )
}
export default NotificationComponent