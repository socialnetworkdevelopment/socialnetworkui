import React, { useState } from 'react'
import "./uploaderFiles.css"
import attach from "../../images/uploaderFiles/attach.svg"
import AppConfig from '../../AppConfig';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

/**
 * Загрузка изображений на сервер
 * @props progress - call back метод, должен принимать одно число - текущий процент загрузки файла (пример: 5.18, 45.04, 93.4), вызывается на протяжении загрузки
 * @props result - call back метод, должен принимать один аргумент - id загруженного изображения в базу данных(null в случае ошибки во время загрузки), вызывается по окончанию загрузки
 * @returns 
 */
const DivUploadImg = ({progress,result,children,className}) => {
    const [id] = useState(uuidv4);
    const fileSelected = (e) => {
        const files = e.currentTarget.files;
        if(files.length > 0){
            let formData = new FormData();
            formData.append("file",files[0])
    
            let url = AppConfig.filesUrl+"/api";
            axios.post(url,formData,config)
            .then(res=>{
                result(res.data);
            })
            .catch(err =>{
                console.log(err)
                result(null);
            });
        }
    }
    const config = {
        onUploadProgress: progressEvent => { if (progress) progress((progressEvent.loaded*100/progressEvent.total).toFixed(2))}   
    }

  return (
    <div className={className}>
        <input className='uploader-file-input' onChange={fileSelected} type='file' accept="image/*" id={id}></input>
        <label for={id}>
            {children}
        </label>
    </div>
  )
}

export default DivUploadImg;