import axios from 'axios';
import React, { useEffect, useState } from 'react'
import AppConfig from '../AppConfig';

const ImgAutoLoad = ({id,style,className}) => {
    const [imgSrc,SetImgSrc] = useState(require('../images/photoEmpty.jpg'))

    useEffect(()=>{
      if(id === null || id === undefined)
      {
        SetImgSrc(require('../images/photoEmpty.jpg'));
      }
      else{
        let url = AppConfig.filesUrl + `/api?id=${id}`;
        axios.get(url).then(res=>{
          if(res.data.contentType.includes("image/"))
            SetImgSrc(`data:${res.data.contentType};base64,${res.data.data}`);
          else
            SetImgSrc(require('../images/photoEmpty.jpg'));
        })
        .catch(err=>{
          SetImgSrc(require('../images/photoEmpty.jpg'));
        })
      }
    },[id])

  return (
    <img src={imgSrc} style={style} className={className} alt={id}></img>
  )
}
export default ImgAutoLoad;