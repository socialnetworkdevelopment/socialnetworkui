import axios from "axios";
import React from "react";
import AppConfig from "../AppConfig"
import { store } from "../store";
const ButtonOpenChatByFriendId = ({friendId,children}) => {
    const openChat = (e) => {
        const selfUserId = store.getState().session.currentUserId;

        axios
        .get(AppConfig.chatServerUrl+`/api/chats/getByParticipants?participantId1=${selfUserId}&participantId2=${friendId}`)
        .then(resp=>{
            console.log(resp);

            window.location.replace(`/chat/${resp.data.id}`)

        })
        .catch(err=>{
            axios
            .post(AppConfig.chatServerUrl+`/api/chats`,{createdBy:selfUserId,сompanionId:friendId})
            .then(resp=>{
                if(resp.status === 201)
                    window.location.replace(`/chat/${resp.data}`)
                console.log(resp);
            })
            .catch(err=>{
                alert(err.response.data.Message);
            })
        })
    }

    return <button onClick={openChat} className="button-inherit">{children}</button>
}
export default ButtonOpenChatByFriendId;