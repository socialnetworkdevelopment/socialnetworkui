import React, { useEffect, useState } from "react";
import NavigationComponent from "../components/navigation/navigation";
import "./profile.css"
import axios from "axios";
import AppConfig from "../AppConfig";
import FriendComponent from "./friendComponent";
import { store } from "../store";
import Posts from "../posts/Posts";
import {useParams} from "react-router-dom";
import img404 from '../images/profile/img404.jpg'
import editIcon from '../images/profile/edit.svg' 

import ButtonOpenChatByFriendId from "../components/buttonOpenChatByFriendId";
import ImgAutoLoad from "../components/imgAutoLoad";
import Header from "../components/header/header";

const ProfilePage = ()=>{
    const { id } = useParams()
    const [state,setState] = useState({
        user:{},
        friends:[]
    })

    let isSelfPage = false;
    if(id === undefined || id == store.getState().session.currentUserId)
        isSelfPage = true;

    useEffect(()=>{
        let userId = id;

        if(id === undefined)
            userId = store.getState().session.currentUserId;

        Promise.all([
            axios.get(AppConfig.usersUrl + `/api/users/${userId}`),
            axios.get(AppConfig.usersUrl + `/api/users/${userId}/friends`)
        ])
        .then((results)=>{
            setState({...state,
                user:results[0].data,
                friends:results[1].data["friends"]
            })
        });
    },[id])


    let buttonSendMessage;
    if(!isSelfPage)
        buttonSendMessage = <ButtonOpenChatByFriendId friendId={id}>Send message</ButtonOpenChatByFriendId>
            // <a href="/allChats"><button className="profile-button-send-message button-inherit">Send message</button></a>;

    let buttonShowAllFiends;
    if(isSelfPage)
        buttonShowAllFiends =
            <a href="/friends"><button className="button-inherit">Show All</button></a>;



    let mainContent;
    if(state.user.id !== undefined)
    mainContent = 
        <div className="profile-main-content-container">
            <div className="profile-user-info-container profile-panel">
                <div  className="profile-user-info-avatar">
                    <ImgAutoLoad id={state.user.avatarId}/>
                </div>
                <div className="profile-user-info-data">
                    <h3>{state.user.firstName} {state.user.lastName}</h3>
                    
                    {state.user.city?
                    <div  className="profile-user-info-data-location-container">
                        <img/>
                        <label>{state.user.city}</label>
                    </div>
                    :""}

                    {state.user.age?
                    <div className="profile-user-info-data-age-container">
                        <img/>
                        <label>{state.user.age} years old</label>
                    </div>
                    :""}
                    
                    {state.user.about?
                        <>
                            <h3>About</h3>
                            <label>{state.user.about}</label>
                        </>
                    :""}
                    
                    {buttonSendMessage}
                </div>
                <div className="profile-user-info-buttons">
                    {isSelfPage?    
                    <a href="/settings"title="Изменить личные данные">
                        <img src={editIcon}></img>
                    </a>
                    :""}
                </div>
            </div>
            <div className="profile-posts-friends-container">
                <div className="profile-posts-container profile-panel">
                <Posts userId={state.user.id}/>
                </div>
                {state.friends.length > 0 ?
                <div className="profile-friends-container profile-panel">
                    <div className="profile-friends-header">
                        <h3>Friends</h3>
                        {buttonShowAllFiends}
                    </div>
                    <div className="profile-friends-list">
                        {state.friends.map((item)=>{
                            return <FriendComponent key={item.id} user={item}/>
                        })}
                    </div>
                </div>
                :""}
            </div>
        </div>;
    else      
    mainContent = 
    <div className="profile-main-content-container">
        <div className="profile-not-found-container">
            <h1>No results found</h1>
            <img src={img404}></img>
        </div>
    </div>;

    return (
    <div className="common-background-content-page">
        <Header/>
        <div className="common-content-page-container">
            <NavigationComponent />
            {mainContent}
        </div>
    </div>);
};
export default ProfilePage;