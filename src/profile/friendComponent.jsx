import React from "react";
import "./profile.css"
import ImgAutoLoad from "../components/imgAutoLoad";

export default class FriendComponent extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(        
            <a href={`/profile/${this.props.user.id}`} className="profile-friends-list-item">
                <div className="profile-friends-list-item-avatar">
                    <ImgAutoLoad id={this.props.user.avatarId}/>
                </div>
                <label>{this.props.user.firstName}</label>
            </a>
        );
    }
}