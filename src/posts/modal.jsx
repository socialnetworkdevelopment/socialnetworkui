import React from 'react';
import classes from './modal.css';
import PostIdPage from './PostIdPage';

const Modal = (props) => {
    //console.log(props.num)
  return (
    <div className={classes.myModal}>
      
      <PostIdPage postnum ={props.num}/>
      <button onClick={props.onClose}>Close</button>
    </div>
  );
}

export default Modal;