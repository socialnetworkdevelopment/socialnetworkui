import React from 'react';
import CommItem from "./commitem";


const CommList = ({comments, title, remove}) => {

    if (!comments.length) {
        return (
            <div style={{textAlign: 'center'}}>
                No comments..
            </div>
        )
    }
    const reversedComments = comments.slice().reverse();
    return (
        <div>
            <div style={{textAlign: 'center'}}>
                {title}
            </div>
            <div >
                {reversedComments.map((comm) =>
                    <div>
                        <CommItem remove={remove} comm={comm}  />
                    </div>
                )}
            </div>
        </div>
    );
};

export default CommList;
