import React from 'react';
import MyButton from './MyButton';
import classes from './postitem.module.css';
import {useNavigate } from 'react-router-dom';
import Moment from 'moment';
import ProfileMini from './profile_mini';
import { store } from "../store";

const CommItem = (props) => {
    
    const router = useNavigate()
    

    return (
        <div className={classes.comm}>
            <div >
            <ProfileMini id = {props.comm.authorId}/>
            <div className={classes.post_date}>
               
               Time create : {Moment(props.comm.createDate).local().format('dddd, MMMM Do YYYY, h:mm:ss')}
                
               </div>
                <div>    {props.comm.text}</div>
                
                
            </div>
            {
            props.comm.authorId == store.getState().session.currentUserId ?
            <div style={{textAlign: 'right'}}>
               
                <MyButton onClick={() => props.remove(props.comm)}>
                    Delete
                </MyButton>
            </div>
            :""
            }
            
        </div>
    );
};

export default CommItem;
