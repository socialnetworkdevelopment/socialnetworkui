import React, {useEffect, useRef, useState} from 'react';
import PostService from './postservice';
import PostList from './postlist';
import MyButton from './MyButton';
import MyInput from './MyInput';
import axios from "axios";
import { store } from "../store";
import {useParams} from "react-router-dom";
import ImgAutoLoad from "../components/imgAutoLoad";
import ButtonUploadImg from "../components/uploaderFiles/buttonUploadImg";
import AppConfig from "../AppConfig";
import NavigationComponent from '../components/navigation/navigation';
import Header from '../components/header/header';

const News = (props) => {

  
   
    
    const [postsNews, setPosts] = useState([])

    

    useEffect(()=>{
        fetchposts()
        
    },[])
    

   

    async function fetchposts() {
        let userId = parseInt(store.getState().session.currentUserId);
        const response =  await PostService.getFriendsById(userId);
        //console.log(response.data)
        const post1 = response.data.sort((a, b) => a.id - b.id);
        //console.log(post1)
        setPosts([...postsNews, ...post1])
        
        //console.log(postsNews)
    }

    // const sortPosts = () => {
    //     const sortedPosts = [...postsNews].sort((a, b) => a.id - b.id);
    //     console.log(sortedPosts)
    //     setPosts(sortedPosts);
    //   };
 
    
  
    // Обработка прогресса загрузки файла
        
    const [imgId,setImgId] = useState(null)
    const callBackProgress = (percent)=>{
            console.log(`Загружено ${percent}%`);
    }
    // Получение id загруженного файла из БД
    const callBackUploadResult = (newIdImage)=>{
        console.log("Id загруженного изображения: "+newIdImage);
            
        setImgId(newIdImage);
  
    }

   

    return (
    <div className="common-background-content-page">
        <Header/>
        <div className="common-content-page-container">
            <NavigationComponent />            
            {/* <ButtonUploadImg result={callBackUploadResult} progress={callBackProgress}></ButtonUploadImg>

            <div style={{overflow:"hidden",width:"6em",height:"6em",borderRadius:"100%",margin:"2em"}}>
                <ImgAutoLoad id={imgId} style={{width: "100%",height: "100%", objectFit:"cover"}}></ImgAutoLoad>
            </div> */}
            <div style={{width:"60%"}}>
                <PostList   posts={postsNews} title=""/>
            </div>
        </div>
    </div>
    );
}

export default News;
