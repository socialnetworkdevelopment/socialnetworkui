import React, {useContext} from 'react';
import {Route, Router, Routes} from "react-router-dom";
import PostIdPage from './PostIdPage';
import Posts from './Posts';

const AppRouter = () => {
     return (
        <Routes>
                <Route path="/posts" element={<Posts/>} />
                <Route path="/posts/:id" element={<PostIdPage/>} />
        </Routes>
        );
};

export default AppRouter;
