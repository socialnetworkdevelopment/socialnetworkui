import React, {useEffect, useState} from 'react';
import MyButton from './MyButton';
import classes from './postitem.module.css';
import {useNavigate } from 'react-router-dom';
import Modal from './modal';
import ImgAutoLoad from "../components/imgAutoLoad";
import Moment from 'moment';
import ProfileMini from './profile_mini';
import { store } from "../store";
import { ReactComponent as LikeSVG } from "../images/svg/like.svg";
import { ReactComponent as DislikeSVG } from "../images/svg/dislike.svg";
import PostService from './postservice';

const PostItem = (props) => {
    
    
    const router = useNavigate()
    const [isModalOpen, setIsModalOpen] = useState(false); // Состояние для отображения модального окна

    const [likesCount, setLikesCount] = useState();
    const [dislikesCount, setDislikesCount] = useState();

    // useEffect(async () => {
    //     let resp = await PostService.getLikes(props.post.id);
    //     if (resp.status == 200) setLikesCount(resp.data);
    //     resp = await PostService.getDislikes(props.post.id);
    //     if (resp.status == 200) setDislikesCount(resp.data);
    // }, [props.post])

    useEffect(()=>{
        fetchlikes()
    },[])
    
    async function fetchlikes() {
        let resp = await PostService.getLikes(props.post.id);
        if (resp.status === 200) setLikesCount(resp.data);
        resp = await PostService.getDislikes(props.post.id);
        if (resp.status === 200) setDislikesCount(resp.data);
    }

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const createdDate = new Date().toLocaleDateString('en-US', {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  });
    const setLike = async () => {
        const resp = await PostService.setLike(props.post.id);
        if (resp.status === 200) {
            let resp = await PostService.getLikes(props.post.id);
            if (resp.status === 200) setLikesCount(resp.data);
            resp = await PostService.getDislikes(props.post.id);
            if (resp.status ===200) setDislikesCount(resp.data);
        }
    }

    const setDislike = async () => {
        const resp = await PostService.setDislike(props.post.id);
        if (resp.status === 200) {
            let resp = await PostService.getLikes(props.post.id);
            if (resp.status === 200) setLikesCount(resp.data);
            resp = await PostService.getDislikes(props.post.id);
            if (resp.status === 200) setDislikesCount(resp.data);
        }
    }

    return (
       <div>

            {
            props.post.authorId == store.getState().session.currentUserId ?
            <div style={{textAlign: 'right'}}>
            <MyButton onClick={() => props.remove(props.post)} >
                Delete
            </MyButton>
             </div>
            :""
            }
            
            <div className={classes.post}>
            <div >
            <ProfileMini id = {props.post.authorId}/>
            <div className={classes.post_date}>
                   
                    
                                
                 Published... : {Moment(props.post.createDate).local().format('dddd, MMMM Do YYYY, h:mm:ss')}

                </div>

                <div className> {props.post.text} </div>
               
                <div style={{overflow:"hidden",margin:"2em"}}>
                <div>{props.post.imageId == null ? (
                <div></div>
                 ) : (
                 <ImgAutoLoad id={props.post.imageId} style={{width: "100%",height: "100%", objectFit:"cover"}}></ImgAutoLoad> 
                    )}
                </div>
                               
                </div>

               
            </div>
                   
            </div>

            <div className={classes.reactionBlock}>
                <div className={classes.reactionPart}>
                    <div className={classes.reactionImg} onClick={setLike}>
                        <LikeSVG className={classes.reactionImgSvg } />
                    </div>
                    <span className={classes.reactionCount}>{likesCount}</span>
                </div>
                <div className={classes.reactionPart}>
                    <div className={classes.reactionImg}
                        onClick={setDislike} style={{marginTop:'12px'}}>
                        <DislikeSVG className={classes.reactionImgSvg} />
                    </div>
                    <span className={classes.reactionCount}>{dislikesCount}</span>
                </div>
            </div>
            <div style={{ padding: '0 0 8px 0' }}>
                               
                 <MyButton onClick={openModal} >
                  Show comments
                  </MyButton>
                 {isModalOpen && <Modal onClose={closeModal} num={props.post.id}/>}
                
                 
                </div>
            </div>
       
        
    );
};

export default PostItem;
