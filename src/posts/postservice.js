import axios from "axios";
import { store } from "../store";
export default class PostService {
    
    static async getAll(id) {
        const response = await axios.get('https://localhost:7003/Posts/getAll/'+id, {
            
        })
        return response;
    }

   

    static async getById(id) {
        const response = await axios.get('https://localhost:7003/Posts/'+id)
        return response;
    }

    static async getFriendsById(id) {
        const response = await axios.get('https://localhost:7003/Posts/getAllFriendsPosts/'+id)
        return response;
    }

    static async getCommentsByPostId(id) {
        const response = await axios.get(`https://localhost:7003/${id}/Comments/getAll`)
        return response;

        
    }
    static async getLikes(id) {
        const response = await axios.get('https://localhost:7003/reaction/' + id + '/like')
        return response;
    }

    static async getDislikes(id) {
        const response = await axios.get('https://localhost:7003/reaction/' + id + '/dislike')
        return response;
    }

    static async setLike(postId) {
        const userId = parseInt(store.getState().session.currentUserId);
        const response = await axios.post('https://localhost:7003/reaction/like', { postId, userId });
        return response;
    }

    static async setDislike(postId) {
        const userId = parseInt(store.getState().session.currentUserId);
        const response = await axios.post('https://localhost:7003/reaction/dislike', { postId, userId });
        return response;
    }
}
