import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import PostService from './postservice';
import PostItem from "./postitem";
import axios from "axios";
import CommList from './commlist';
import MyButton from './MyButton';
import MyInput from './MyInput';
import classes from './postitem.module.css';
import { store } from "../store";

const PostIdPage = (props) => {
    //console.log(props.postnum)
    let id  = props.postnum

    //console.log(id)
    const [text, setText] = useState([])
    const [post, setPost] = useState({});
    const [comments, setComments] = useState([]);
    

    useEffect(() => {
       
        fetchPostById()
        fetchComments()
    },[])

    async function fetchComments() {
     
        const response = await PostService.getCommentsByPostId(id)
        setComments(response.data.filter(p => p.postId === parseInt(id)));
       
    }
  
    
    async function fetchPostById() {
        
        const response = await PostService.getById(id)
        setPost(response.data);
    }

    const removeComm = (comm) => {
       
        axios.delete(`https://localhost:7003/${id}/Comments/${comm.id}`)
        setComments(comments.filter(c => c.id !== comm.id))
      
    }

    const addNewComm = (e) => {
        e.preventDefault()
        let userId = parseInt(store.getState().session.currentUserId);
        //console.log( userId);
        const newComm = {
            
            text,
            userId: userId,
            postId: parseInt(id),
            authorId:parseInt(store.getState().session.currentUserId),
            createDate: new Date().toISOString()
        }
        console.log( newComm);
        
        axios.post(`https://localhost:7003/${id}/Comments`, 
        newComm,{
            headers: {
              "Content-Type": "application/json",
            }
          })
            .then(response => {
                console.log('Success:', response);
                //newTextId = response.data;
                const commNew = { text: newComm.text, id: response.data, 
                    userId: userId,authorId:newComm.authorId,postId: id,createDate:newComm.createDate};
                    //console.log( commNew);
                    setComments(prevposts=>[...prevposts, commNew])
                    
            })
            .catch((error) => {
                console.error('Error:', error);
            });
        setText('')
    }

    
    return (
        <div>
            
            {/* <div className={classes.post}>
            
                <div> {post.id}  {post.postId} {post.text}</div>
                <div className={classes.post_date}>
                {post.userId}   {post.createDate}
                </div>
            
           
            </div> */}
             
             <MyInput
                value={text}
                onChange={e => setText(e.target.value)}
                type="text"
                placeholder="Input comment..."
            />
            <MyButton onClick={addNewComm}> Add comment
                </MyButton>
            <div>
            <CommList  remove={removeComm} comments={comments}/>
            </div>
        </div>
    
    );
};

export default PostIdPage;
