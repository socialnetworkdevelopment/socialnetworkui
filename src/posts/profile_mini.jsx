import React, { useEffect, useState } from "react";
import NavigationComponent from "../components/navigation/navigation";
import "./profile_mini.css"
import axios from "axios";
import AppConfig from "../AppConfig";
import FriendComponent from "../profile/friendComponent";
import { store } from "../store";
import Posts from "./Posts";
import {useParams} from "react-router-dom";
import img404 from '../images/profile/img404.jpg'
import editIcon from '../images/profile/edit.svg' 

import ButtonOpenChatByFriendId from "../components/buttonOpenChatByFriendId";
import ImgAutoLoad from "../components/imgAutoLoad";

const ProfileMini = (id)=>{
   
    //console.log(id)
    const [state,setState] = useState({
        user:{},
        
    })

    let isSelfPage = false;
    if(id === undefined || id == store.getState().session.currentUserId)
        isSelfPage = true;

    useEffect(()=>{
        //let userId = id;
        
        // if(id === undefined)
        //     userId = store.getState().session.currentUserId;

        Promise.all([
            axios.get(AppConfig.usersUrl + `/api/users/${id.id}`),
            
        ])
        .then((results)=>{
            setState({...state,
                user:results[0].data
            })
        });
    },[id])

 

    let mainContent;
    if(state.user.id !== undefined)
    mainContent = 
        <div >
            <div className="post-user-info-container profile-panel">
                <div  className="post-user-info-avatar">
                    <ImgAutoLoad id={state.user.avatarId}/>
                </div>
                <div >
                    {state.user.firstName}  {state.user.lastName}
                                   
                </div>
               
            </div>
            
        </div>;
    else      
    mainContent = 
    <div className="profile-main-content-container">
        <div className="profile-not-found-container">
            <h1>No results found</h1>
            <img src={img404}></img>
        </div>
    </div>;

    return (
    <div >
        <div >
            {mainContent}
        </div>
    </div>);
};
export default ProfileMini;