import React from 'react';
import PostItem from "./postitem";


const PostList = ({posts, title, remove}) => {

    if (!posts.length) {
        return (
            <h1 style={{textAlign: 'center'}}>
                No posts..
            </h1>
        )
    }
    const reversedPosts = posts.slice().reverse();
    return (
        <div>
            <h1 style={{textAlign: 'center'}}>
                {title}
            </h1>
            <div >
                {reversedPosts.map((post) =>
                    <div>
                        <PostItem remove={remove} post={post}  />
                    </div>
                )}
            </div>
        </div>
    );
};

export default PostList;
