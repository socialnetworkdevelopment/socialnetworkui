import React, {useEffect, useRef, useState} from 'react';
import PostService from './postservice';
import PostList from './postlist';
import MyButton from './MyButton';
import MyInput from './MyInput';
import axios from "axios";
import { store } from "../store";
import {useParams} from "react-router-dom";
import ImgAutoLoad from "../components/imgAutoLoad";
import ButtonUploadImg from "../components/uploaderFiles/buttonUploadImg";

const Posts = (props) => {
    const { id } = useParams()
    const [posts, setPosts] = useState([])
    const [text, setText] = useState([])
    const [comm, setComments] = useState([]);
    useEffect(()=>{
        fetchposts()
    },[])
    
    async function fetchposts() {
        let userId = id;
        if (id == null){
            userId = parseInt(store.getState().session.currentUserId);
        } else{
             userId = id;
        }
        
        //console.log(userId)
        const response = await PostService.getAll(userId);
        
        setPosts([...posts, ...response.data])
        //console.log(posts)
    }

    const addNewPost = (e) => {
        e.preventDefault()
        let userId = id;
        if (id == null){
            userId = parseInt(store.getState().session.currentUserId);
        } else{
             userId = id;
        }
        //let userId = parseInt(store.getState().session.currentUserId);
        const newPost = {
            
            text,
            userId: userId,
            AuthorId:parseInt(store.getState().session.currentUserId),
            imageId: imgId,
            createDate: new Date().toISOString(),
            comments:[]
        }
        
        console.log(newPost)
        axios.post('https://localhost:7003/Posts', 
            newPost,{
            headers: {
              "Content-Type": "application/json",
            }
          })
            .then(response => {
                console.log('Success:', response);
                //newTextId = response.data;
                const postNew = { text: newPost.text, id: response.data, 
                    userId: userId,AuthorId:newPost.AuthorId,imageId:newPost.imageId,createDate:newPost.createDate};
                    console.log(postNew)
                setPosts(prevposts=>[...prevposts, postNew])
                })
            .catch((error) => {
                console.error('Error:', error);
            });
        setText('')
        setImgId();
    }
 
    async function removePost(post)  {
        //console.log(post)
        const response =  await PostService.getCommentsByPostId(post.id)
        const p = response.data.filter(p => p.postId === parseInt(post.id))
        
        for(var i = 0 ; i < p.length ; i++){
            await axios.delete(`https://localhost:7003/${post.id}/Comments/${p[i].id}`)
        }

        await axios.delete('https://localhost:7003/Posts/'+post.id)
        setPosts(posts.filter(p => p.id !== post.id))
    }
  
    // Обработка прогресса загрузки файла
        
    const [imgId,setImgId] = useState(null)
    const callBackProgress = (percent)=>{
            console.log(`Загружено ${percent}%`);
    }
    // Получение id загруженного файла из БД
    const callBackUploadResult = (newIdImage)=>{
        console.log("Id загруженного изображения: "+newIdImage);
            
        setImgId(newIdImage);
  
    }

   

    return (
        <div >
            <MyInput
                value={text}
                onChange={e => setText(e.target.value)}
                type="text"
                placeholder="Add post..."
            />
            <ButtonUploadImg result={callBackUploadResult} progress={callBackProgress}></ButtonUploadImg>

            <div style={{overflow:"hidden",width:"6em",height:"6em",borderRadius:"100%",margin:"2em"}}>
                <ImgAutoLoad id={imgId} style={{width: "100%",height: "100%", objectFit:"cover"}}></ImgAutoLoad>
            </div>
            
              <MyButton onClick={addNewPost}> Add post </MyButton>  
            <PostList  remove={removePost} posts={posts} title=""/>
           
        </div>
    );
}

export default Posts;
