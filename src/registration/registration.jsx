import React from 'react';
import './registration.css'
import axios from 'axios';
import { useNavigate , redirect } from 'react-router-dom';
import AppConfig from "../AppConfig";


class RegistrationPage extends React.Component
{
    constructor(props) {
        super(props);
        
    }
      
    registrationUser=(e)=>{
        e.preventDefault()
        let form = document.forms.newUser;

        let login = form.login.value;
        let email = form.email.value;
        let pass1 = form.password.value;
        let pass2 = form.password2.value;

        console.log(login);
        if(login == "" || email == "" || pass1 == "")
        { 
            window.alert("Поля должны быть заполнены");
            return;
        }
        if(pass1 !== pass2){
            window.alert("Пароли должны совпадать");
            return;
        }

        let url = AppConfig.identityServerUrl +"/auth/createUser?login="+login+"&pass="+pass1+"&email="+email;
        axios.post(url)
        .then((req)=>{
            if(req.status === 201)
                window.location.replace("/login?login="+login);
        })
        .catch((err)=>{
            if(err.response.status === 400)
                window.alert(err.response.data);
            if(err.response.status === 500)
                window.alert(err.response.data)
            console.log(err);
        });
    }
    render(){
        return(
            <div className="common-background">
                <div className='common-content-container'>
                    <h1>
                        <label>Social</label>
                        <label className='common-header-word-text-color'>Buddy</label>
                        <label>Chat</label>
                    </h1>
                    <h2>Sign up</h2>
                    <form name='newUser'>
                        <div className='common-input-container'>
                            <label>Login</label>
                            <input placeholder='Enter your login' name='login' required></input>
                        </div>
                        <div className='common-input-container'>
                            <label>Email</label>
                            <input placeholder='Enter your email' name='email'></input>
                        </div>
                        <div className='common-input-container'>
                            <label>Password</label>
                            <input placeholder='Enter your password' type='password' name='password'></input>
                        </div>
                        <div className='common-input-container'>
                            <label>Repeat password</label>
                            <input placeholder='Repeat your password' type='password' name='password2'></input>
                        </div>
                        <div className='registrationPageButtonContainer'>
                            <button onClick={this.registrationUser}>Sign up</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
    
}
export default RegistrationPage;