import React, { useEffect, useState } from 'react'
import NavigationComponent from '../components/navigation/navigation';
import '../profile/profile.css'
import './profileSettings.css'
import axios from 'axios';
import AppConfig from '../AppConfig';
import { store } from '../store';
import ImgAutoLoad from '../components/imgAutoLoad';
import DataInputField from '../components/dataInputField';
import DataTextAreaField from '../components/dataTextAreaField';
import DivUploadImg from '../components/uploaderFiles/divUploadImg';
import Header from '../components/header/header';

const ProfileSettings = () => {
    const [user,setUser] = useState({});
    const userId = store.getState().session.currentUserId;

    useEffect(()=>{

        axios.get(AppConfig.usersUrl + `/api/users/${userId}`)
        .then((res)=>{
            setUser(res.data);
            console.log(res);
        })
    },[])

    const onSubmitForm = (e) => {
        e.preventDefault();
        const updateFields = {
            firstName:user.firstName,
            lastName:user.lastName,
            age:user.age,
            city:user.city,
            about:user.about,
            avatarId:user.avatarId
        }
        console.log(updateFields);
        axios.put(AppConfig.usersUrl+`/api/users/${userId}`,updateFields)
        .then((res)=>{
            console.log(res);
        })
        .catch((er)=>{
            console.log(er);
        })
    }

    const newAvatarUploading = (id) => {
        axios.patch(AppConfig.usersUrl+`/api/users/${userId}`, id, { 'headers': {
            'Content-Type': 'application/json; charset=utf-8'
          }})
        .then((res)=>{
            axios.delete(AppConfig.filesUrl+`/api?id=${user.avatarId}`);
            setUser({...user,avatarId:id})
        })
        .catch((er)=>{
            axios.delete(AppConfig.filesUrl+`/api?id=${id}`);
        })
    }

  return (
    <div className="common-background-content-page">
        <Header/>
        <div className="common-content-page-container">
            <NavigationComponent />
            <div className="profile-main-content-container">
                <div className="profile-user-info-container profile-panel">
                    <DivUploadImg result={newAvatarUploading}  className="profile-user-info-avatar profile-settings-button-change-avatar">
                        <ImgAutoLoad id={user.avatarId}/>
                    </DivUploadImg>
                        <form  className="profile-user-info-data" onSubmit={onSubmitForm}>
                            <DataInputField label="First name" placeholder="Enter your name" required
                                value={user.firstName} onChange={(value)=>{setUser({...user,firstName:value})}}/>
                            <DataInputField label="Last name" placeholder="Enter your last name" 
                                value={user.lastName} onChange={(value)=>{setUser({...user,lastName:value})}}/>
                            <DataInputField label="Your city" placeholder="Enter your city" 
                                value={user.city} onChange={(value)=>{setUser({...user,city:value})}}/>
                            <DataInputField label="Your age" placeholder="Enter your age"  type="number"
                                value={user.age} onChange={(value)=>{setUser({...user,age:value})}}/>
                            <DataTextAreaField label="About" placeholder="Tell us about yourself" 
                                value={user.about} onChange={(value)=>{setUser({...user,about:value})}}/>

                            <button type='submit' className='button-inherit'>Save</button>
                        </form>
                </div>
            </div>
        </div>
    </div>);
}
export default ProfileSettings;