import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import axios from 'axios';
import App from './App';
import ReactDOM from 'react-dom/client';
import React from 'react';
import { store, persistor } from './store';

const rootElement = document.getElementById('root');

// Instead of ReactDOM.render, use createRoot to render your app
const root = ReactDOM.createRoot(rootElement);

root.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <React.Suspense>
                <App />
            </React.Suspense>
        </PersistGate>
    </Provider>
);

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    const token = store.getState().session.token;
    config.headers.Authorization = `Bearer ${token}`;
    return config;
});