import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

// Define the initial state of the store
const initialState = {
    session: {
        token: null,
    },
};

// Define the reducer function
function rootReducer(state = initialState, action) {
    switch (action.type) {
        case 'UPDATE_TOKEN':
            return {
                ...state,
                session: {
                    token: action.payload.token,
                    currentUserId: action.payload.currentUserId,
                },
            };
        default:
            return state;
    }
}

// Define the persistConfig object
const persistConfig = {
    key: 'root',
    storage,
};

// Create a persisted reducer using persistReducer
const persistedReducer = persistReducer(persistConfig, rootReducer);

// Create a Redux store using configureStore and pass in the persistedReducer
const store = configureStore({
    reducer: persistedReducer,
    serializableCheck: false,
    middleware: getDefaultMiddleware({
        serializableCheck: false,
    }),
});

// Create a new persistor object
const persistor = persistStore(store);

export { store, persistor };